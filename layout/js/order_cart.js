var maxQuantity = -1;

// Get seleted row(s) content in DataTable
function getSelectedRows(dataTable) {
	return dataTable.row('.selected').data();
}

// Select row in DataTable
$('#productTable tbody').on('click', 'tr', function () {
	if ($(this).hasClass('selected')) {
		$(this).removeClass('selected');
	} else {
		prdTable.$('tr.selected').removeClass('selected');
		$(this).addClass('selected');
	}
});

$(document).delegate('.qty', 'change', function () {
	var parent = $(this).parents('tr'),
		maxQuantity = parseInt(parent.find('.maxQty').val()) || 0,
		quantity = parseInt(parent.find('.qty').val()) || 0,
		price = parseInt(parent.find('.prc').val()) || 0;

	if (quantity > maxQuantity) { // Số lượng mua > số lượng trong kho hiện có
		quantity--;
		parent.find('.qty').val(maxQuantity);
	}

	parent.find('.total').val(quantity * price);
});

function addNewProduct(productData, quantity) {
    quantity = (quantity > productData.quantity ? productData.quantity : quantity);
	var htmlRow = "<tr class='cartSanPham' data-id='"+ productData.id +"'  >" +
		"<td>" + productData.id + "</td>" +
		"<td>" + productData.image + "</td>" +
		"<td>" + productData.name + "</td>" +
        "<td>" +
		"<div class='form-line'>" +
		"<input type='number' class='form-control qty' value=" + quantity + " max=" + productData.quantity + ">" +
		"</div>" +
		"</td>" +
        "<td class='hidden'>" +
		"<div class='form-line'>" +
		"<input type='number' class='form-control maxQty' value=" + productData.quantity + ">" +
		"</div>" +
		"</td>" +
		"<td>" +
		"<div class='form-line'>" +
		"<input type='text' class='form-control prc' value=" + productData.price + " readonly>" +
		"</div>" +
		"</td>" +
		"<td>" +
		"<div class='form-line'>" +
		"<input type='text' class='form-control total' value=" + productData.price * quantity + " readonly>" +
		"</div>" +
		"</td>" +
		"<td>" +
		"<a onclick='checkalert()' class='btn btn-xs btn-danger btn-raised'><i class='material-icons'>delete</i></a>" + //Code for DELETE BUTTON HERE
		"</td>" +
		"</tr>";
	$('#tableCart').append(htmlRow);
}

function inputQuantity() {
    $('#selectProductQuantityModal').modal('show');
}

function selectProduct() {
	var row = getSelectedRows(prdTable),
		quantity = $('#quantityInputModal').val();

	row['total'] = row.price * quantity;
	addNewProduct(row, quantity)

	// Close modal
	$('#selectProductQuantityModal').modal('hide');
	$('#selectProductModal').modal('hide');
}