//====================CUSTOMER DATATABLE=========================
$('#customerTable tbody').on( 'click', 'tr', function () {
    if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
    }
    else {
        cusTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
} );

function chooseCustomer() {
    var row = getSelectedRows(cusTable);
    
    // Fill data in textbox here
    $('#fullname').val(row.fullname);
    $('#email').val(row.email);
    $('#phone').val(row.phone);
    $('#customerId').val(row.id);

    // Close modal
    $('#chooseCustomerModal').modal('hide');
}
//====================CUSTOMER DATATABLE=========================

//====================VOUCHER DATATABLE=========================
$('#voucherTable tbody').on( 'click', 'tr', function () {
    if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
    }
    else {
        vouTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
} );

function chooseVoucher() {
    var row = getSelectedRows(vouTable);
    
    // Fill data in textbox here
    $('#voucher').val("[" + row.code + "] " + row.title);
    $('#voucherId').val(row.id);

    // Close modal
    $('#chooseVoucherModal').modal('hide');
}
//====================VOUCHER DATATABLE=========================