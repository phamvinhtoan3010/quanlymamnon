-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2018 at 05:05 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qlmn`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `Tenalbum` text COLLATE utf8_unicode_ci NOT NULL,
  `Giodang` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Malop` int(11) NOT NULL,
  `Mota` text COLLATE utf8_unicode_ci NOT NULL,
  `Soluong` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `Tenalbum`, `Giodang`, `Malop`, `Mota`, `Soluong`) VALUES
(3, 'Hoạt động vui chơi ngoài trời với các bé mầm non!', '2018-04-20 00:00:00', 3, '“Chúng tớ ra sân tập thể dục và vận động trên sân cỏ thích ghê”\r\n“ Thi đấu giao lưu ngoài trời giữa 2 lớp thật vui”\r\n” Cả lớp cùng quan sát cây bằng lăng và có nhiều phát hiện rất mới mẻ”\r\n“ Thi xem ai vượt qua thử thách nhanh nhất nào”\r\n” Ra sân chơi nhiều trò chơi thú vị lắm”', 7),
(6, 'CÁC HOẠT ĐỘNG DÀNH CHO TRẺ MẦM NON TẠI VAS', '2018-05-03 18:55:36', 2, 'Bé vui Trung thu, Ngày hội đồ chơi của trẻ, Bé vui học an toàn giao thông, Chuyên đề sách, Điện ảnh trong mắt trẻ thơ... là những hoạt động vui học sôi nổi và đầy ý nghĩa của các bé Mầm non tại VAS trong hai tuần vừa qua!', 8);

-- --------------------------------------------------------

--
-- Table structure for table `baidang`
--

CREATE TABLE `baidang` (
  `id` int(11) NOT NULL,
  `Malop` int(11) NOT NULL,
  `Manguoidung` int(11) NOT NULL,
  `Noidung` text COLLATE utf8_unicode_ci NOT NULL,
  `Ngaydang` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `baidang`
--

INSERT INTO `baidang` (`id`, `Malop`, `Manguoidung`, `Noidung`, `Ngaydang`) VALUES
(1, 3, 2, 'Hoạt động vui chơi ngoài trời với các bé mầm non!\r\n\r\n         Hoạt động vui chơi là một trong những hoạt động chủ đạo của trẻ mầm non, nhằm giúp trẻ thỏa mãn nhu cầu vui chơi và phát triển toàn diện ở trẻ. Bên cạnh đó, hoạt động vui chơi còn là phương tiện làm phong phú vốn hiểu biết, kinh nghiệm của trẻ về thế giới xung quanh.\r\n\r\n         Tư duy và sự tập trung ở trẻ mầm non còn rất hạn chế, trẻ không thể tiếp thu các kiến thức một cách bài bản, có hệ thống như trẻ lớn. Vì thế, cần tạo cho trẻ môi trường để trẻ được hoạt động, trải nghiệm, vui chơi, từ đó trẻ có thể tiếp thu kiến thức một cách nhẹ nhàng, tự nhiên hơn. Trẻ học mà chơi, chơi mà học, qua chơi việc học của trẻ trở nên nhẹ nhàng và đạt hiệu quả cao hơn.\r\n         Đa số các trường mầm non tư thục nhỏ hiện nay đều không có không gian cho trẻ hoạt động, các con sinh hoạt chủ yếu trong một không gian chật hẹp nên không có điều kiện ra ngoài hít thở không khí trong lành. Tuy nhiên, ở trường mầm non Hanoi Academy lại khác, các con có cả khu sân cỏ nhân tạo ngoài trời để chơi. Mỗi ngày, từng lớp đều dành thời gian để cho các con ra tắm nắng, chơi trò chơi vận động, rồi thảo luận về một vấn đề gì đó trong hoạt động có chủ đích mà cô đưa ra. Bạn nào cũng có cơ hội để được thể hiện khả năng riêng của mình: bạn thì có tố chất vận động nhanh nhẹn, khéo léo, bạn thì lại có khả năng quan sát tốt, có bạn thì lại tưởng tượng, phán đoán, suy luận rất logic…\r\nhttps://hanoiacademy.edu.vn/hoat-dong-vui-choi-ngoai-troi-voi-cac-be-mam-non/', '2018-04-20 00:00:00'),
(2, 2, 6, 'SÔI NỔI CÁC HOẠT ĐỘNG DÀNH CHO TRẺ MẦM NON TẠI VAS\r\nBÉ VUI TRUNG THU\r\n\r\nTết Trung thu là một trong những ngày lễ hội luôn được các bé yêu thích, chờ đợi. Trong không khí háo hức chào đón tết trung thu, các cơ sở Mầm non VAS đã phối hợp với phụ huynh tổ chức nhiều hoạt động vui chơi, học tập nhằm giúp các bé hiểu được ý nghĩa của ngày lễ này. Nhiều phụ huynh đã thật sự cảm động khi được trực tiếp tham gia các hoạt động này và nhìn thấy các con tự lập và tích cực tham gia.\r\nPhụ huynh của bé Cao Ngọc Bảo Trân, Lớp Pre 2.2, Cơ sở Sunrise chia sẻ: “Tôi luôn yên tâm về cách giáo dục của nhà trường đối với học sinh. Đặc biệt là đối với các bé học sinh khối Chồi, vì các bé không chỉ được chăm sóc về mặt thể chất, rèn luyện kỹ năng, tư duy mà còn được tìm hiểu về văn hóa, xã hội. Điển hình là Ngày Hội vui trung thu mà tôi rất vui khi được tham gia cùng các bé. Qua đó, tôi càng yên tâm, tin tưởng khi cho bé theo học tại trường. Tôi luôn ủng hộ nhà trường và Giáo viên tổ chức nhiều hơn nữa những hoạt động vui chơi bổ ích như thế cho các bé”.\r\n\r\nCHUYÊN ĐỀ SÁCH THÚ VỊ\r\n\r\nĐọc sách rất tốt cho sự phát triển ngôn ngữ và khả năng tưởng tượng phong phú của trẻ. Bên cạnh đó, rèn luyện được thói quen đọc sách, các bé còn học được khả năng tập trung và kỹ năng giải quyết vấn đề thông qua các tình huống trong các câu chuyện.\r\nĐầu tháng 10 vừa qua, cơ sở Lê Quý Đôn đã tổ chức một Chuyên đề Sách với rất nhiều hoạt động bổ ích dành cho trẻ như tham quan nhà sách, Mẹ đọc truyện bé nghe, Đọc sách cùng con… Các bé còn được tự tay làm những cuốn sách với chủ đề mà mình yêu thích!\r\nhttps://www.vas.edu.vn/post/soi-noi-cac-hoat-dong-danh-cho-tre-mam-non-tai-VAS', '2018-05-03 18:55:17'),
(3, 1, 2, 'Một ngày của bé ở trường mầm non Họa Mi quận Tân Bình\r\nGIỜ\r\nHOẠT ĐỘNG\r\nNỘI DUNG\r\n06:45 – 07:30	– Đón trẻ	– Cô quản cho trẻ chơi tự do\r\n– Cô trao đổi với phụ huynh những thông tin cần thiết\r\n07:30 – 08:30	– Vận động thể dục\r\n– Ăn sáng	– 7h30 Thể dục buổi sáng\r\n– 7h45 Ăn sáng, uống sữa\r\n08:30 – 08:45	– Vệ sinh	– Cô rèn trẻ đi vệ sinh và vệ sinh cho trẻ\r\n08:45 – 09:10	– Hoạt động ngoài trời	– Cô tổ chức sinh hoạt vui chơi phát triển thể chất cho trẻ\r\n09:15 – 09:30	– Vệ sinh\r\n– Ăn giữa giờ	– Vệ sinh\r\n– Trẻ uống nước trái cây hoặc ăn nhẹ giữa giờ\r\n09:30 – 10:00	– Hoạt động có chủ đích	– Cô tổ chức hoạt động vui chơi cho trẻ theo kế hoạch\r\n– Trẻ sinh hoạt vui chơi, múa hát với cô hoặc qua đĩa nhạc, TV/DVD\r\n10:00 – 10:15	– Chuẩn bị ăn trưa	– Trẻ rửa tay, lau mặt, đi vệ sinh\r\n10:15 – 11:00	– Ăn trưa	– Trẻ dùng cơm trưa\r\n11:00 – 11:30	– Chuẩn bị ngủ trưa	– Vệ sinh cho trẻ, thay đồ. Chuẩn bị ngủ trưa\r\n11:45 – 14:00	– Ngủ trưa	– Bé nhỏ uống cử sữa phụ (do PH gởi kèm, nếu có)\r\n– Trẻ ngủ trưa\r\n14:00 – 14:15	– Hoạt động nhẹ	– Trẻ thức giấc vận động nhẹ theo nhạc\r\n14:15 – 15:00	– Ăn xế	– Ăn xế Yaourt ( hoặc trái cây) Vệ sinh cho trẻ\r\n15:00 – 16:00	– Montessori	– Cô tổ chức theo kế hoạch cho trẻ\r\n16:00 – 17:00	– Trả trẻ	– Vui chơi tự do, ôn tập.', '2018-05-03 18:35:16');

-- --------------------------------------------------------

--
-- Table structure for table `chuansuckhoe`
--

CREATE TABLE `chuansuckhoe` (
  `id` int(11) NOT NULL,
  `Gioitinh` bit(1) NOT NULL,
  `Thang` int(11) NOT NULL,
  `Tuoi` int(11) NOT NULL,
  `SDD` int(11) NOT NULL,
  `NguycoSDD` int(11) NOT NULL,
  `BThuong` int(11) NOT NULL,
  `NguycoBP` int(11) NOT NULL,
  `Beophi` int(11) NOT NULL,
  `ChieuCao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chuansuckhoe`
--

INSERT INTO `chuansuckhoe` (`id`, `Gioitinh`, `Thang`, `Tuoi`, `SDD`, `NguycoSDD`, `BThuong`, `NguycoBP`, `Beophi`, `ChieuCao`) VALUES
(1, b'0', 12, 1, 8, 9, 10, 11, 12, 73),
(2, b'0', 24, 2, 10, 11, 12, 14, 15, 85),
(3, b'0', 36, 3, 11, 13, 14, 16, 18, 92),
(4, b'0', 48, 4, 13, 14, 16, 19, 21, 99),
(5, b'0', 60, 5, 14, 16, 18, 21, 24, 105),
(6, b'1', 12, 1, 7, 8, 9, 10, 12, 71),
(7, b'1', 24, 2, 9, 10, 12, 13, 15, 83),
(8, b'1', 36, 3, 11, 12, 14, 16, 18, 91),
(9, b'1', 48, 4, 12, 14, 16, 19, 22, 98),
(10, b'1', 60, 5, 14, 16, 18, 21, 25, 105);

-- --------------------------------------------------------

--
-- Table structure for table `diemdanh`
--

CREATE TABLE `diemdanh` (
  `id` int(11) NOT NULL,
  `Mahocsinh` int(11) NOT NULL,
  `Malop` int(11) NOT NULL,
  `Giodiemdanh` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Trangthai` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diemdanh`
--

INSERT INTO `diemdanh` (`id`, `Mahocsinh`, `Malop`, `Giodiemdanh`, `Trangthai`) VALUES
(1, 2, 2, '2018-04-16 00:00:00', 'Có mặt'),
(2, 1, 3, '2018-04-16 00:00:00', 'Có mặt');

-- --------------------------------------------------------

--
-- Table structure for table `hinhanh`
--

CREATE TABLE `hinhanh` (
  `id` int(11) NOT NULL,
  `filename` text COLLATE utf8_unicode_ci NOT NULL,
  `type` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hocsinh`
--

CREATE TABLE `hocsinh` (
  `id` int(11) NOT NULL,
  `Hoten` text COLLATE utf8_unicode_ci NOT NULL,
  `Nickname` text COLLATE utf8_unicode_ci NOT NULL,
  `Gioitinh` bit(1) NOT NULL,
  `Ngaysinh` datetime NOT NULL,
  `Maphuhuynh` int(11) NOT NULL,
  `Malop` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hocsinh`
--

INSERT INTO `hocsinh` (`id`, `Hoten`, `Nickname`, `Gioitinh`, `Ngaysinh`, `Maphuhuynh`, `Malop`) VALUES
(1, 'Võ Thanh Ngân', 'Baby', b'1', '2013-02-12 00:00:00', 3, 3),
(2, 'Huỳnh Gia Bảo', '', b'0', '2014-02-14 00:00:00', 5, 2),
(3, 'Phương Trinh', 'ChinhChinh', b'1', '2015-12-19 00:00:00', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `lop`
--

CREATE TABLE `lop` (
  `id` int(100) NOT NULL,
  `Tenlop` text COLLATE utf8_unicode_ci NOT NULL,
  `Giaovien1` text COLLATE utf8_unicode_ci,
  `Giaovien2` text COLLATE utf8_unicode_ci,
  `Giaovien3` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lop`
--

INSERT INTO `lop` (`id`, `Tenlop`, `Giaovien1`, `Giaovien2`, `Giaovien3`) VALUES
(1, 'Mầm', 'Thảo', 'Hương', 'Trang'),
(2, 'Chồi', 'Loan', 'Hương', 'Hà'),
(3, 'Lá', 'Vân', 'Ngọc', 'Linh');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `Thoigianan` datetime NOT NULL,
  `Malop` int(11) NOT NULL,
  `BuoiSang` text COLLATE utf8_unicode_ci NOT NULL,
  `BuoiTrua` text COLLATE utf8_unicode_ci NOT NULL,
  `BuoiToi` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `Thoigianan`, `Malop`, `BuoiSang`, `BuoiTrua`, `BuoiToi`) VALUES
(1, '2018-04-16 00:00:00', 3, 'Cháo thịt rau củ, quả', 'Cơm tám\r\nThịt gà om nấm\r\nCanh khoai thập cẩm', 'Bún bò rau thơm\r\nHoa quả	'),
(2, '2018-04-17 00:00:00', 3, 'Miến gà Phú Hương', 'Cơm tám	\r\nTôm sốt ngũ liễu\r\nCanh rau ngót nấu thịt', 'Cháo gạo lứt, lạc vừng	\r\nHoa quả	'),
(3, '2018-04-16 00:00:00', 1, 'Bún thịt rau thơm', 'Cơm tấm	 \r\nThịt kho trứng cút\r\nCanh bí đao nấu thịt\r\n \r\n\r\n\r\n\r\n', 'Cháo lươn hành răm\r\nHoa quả'),
(4, '2018-04-17 00:00:00', 1, 'Cháo sườn hầm bí đỏ.', 'Cơm tám	\r\nCanh cua mùng tơi\r\nĐậu om thịt', 'Bún canh chua nấu thịt bằm.'),
(5, '2018-04-16 00:00:00', 2, 'Cháo sườn bí đỏ.\r\n\r\n', 'Cơm tám	\r\nĐậu om thịt\r\nCanh cua rau mùng tơi rau đay', 'Phở gà lá chanh\r\nHoa quả'),
(6, '2018-04-17 00:00:00', 2, 'Cháo thịt bằm củ rền', 'Cơm tám\r\nBò sốt vang\r\nCanh bí đao nấu thịt	', 'Thịt bò hầm rau củ, bánh mỳ gối\r\nHoa quả\r\n'),
(7, '2018-04-18 00:00:00', 3, 'Mỳ trũ nấu thịt', 'Cơm tám	\r\nThịt bò hầm hành tây, carot.	\r\nCanh bầu nấu thịt', 'Xôi đậu xanh nước cốt dừa\r\nHoa quả	'),
(8, '2018-04-18 00:00:00', 1, 'Miến gà Phú Hương', 'Cơm tám\r\nSúp thịt cá\r\nCanh chua thả giá', 'Thịt bò hầm rau củ, bánh mỳ gối\r\nHoa quả'),
(9, '2018-04-18 00:00:00', 2, 'Bún thịt gà rau thơm\r\n', 'Cơm tám	\r\nThịt kho tàu\r\nCanh rau cải cá rô	', 'Cháo gạo lứt, lạc vừng\r\nHoa quả\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `nguoidung`
--

CREATE TABLE `nguoidung` (
  `id` int(11) NOT NULL,
  `tendangnhap` text COLLATE utf8_unicode_ci NOT NULL,
  `Matkhau` text COLLATE utf8_unicode_ci NOT NULL,
  `Hoten` text COLLATE utf8_unicode_ci NOT NULL,
  `Gioitinh` bit(1) NOT NULL,
  `Ngaysinh` datetime NOT NULL,
  `SDT` int(11) NOT NULL,
  `Email` text COLLATE utf8_unicode_ci NOT NULL,
  `Diachi` text COLLATE utf8_unicode_ci NOT NULL,
  `Quyentruycap` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nguoidung`
--

INSERT INTO `nguoidung` (`id`, `tendangnhap`, `Matkhau`, `Hoten`, `Gioitinh`, `Ngaysinh`, `SDT`, `Email`, `Diachi`, `Quyentruycap`) VALUES
(1, 'dongxanh0211', '123', 'Nguyễn Trọng Thanh Nguyên', b'1', '1996-11-02 00:00:00', 987503445, 'ngtrthnguyen.hcmue@gmail.com', 'Quận 11', 1),
(2, 'giaovien1', '123', 'Nguyễn Thị Thảo', b'1', '1990-05-02 00:00:00', 123456789, 'thao0205@gmail.com', 'Long An', 2),
(3, 'phuhuynh1', '123', 'Trần Văn B', b'0', '1980-03-05 00:00:00', 987654321, '', 'Long An', 3),
(4, 'phuhuynh2', '123', 'Lê An', b'1', '1994-10-24 00:00:00', 1655646190, '', 'Củ Chi', 3),
(5, 'phuhuynh3', '123', 'Võ Chí T', b'0', '1996-01-07 00:00:00', 1675748394, '', 'Long Hải', 3),
(6, 'giaovien2', '123', 'Nguyễn Bé Hương', b'1', '1992-07-18 00:00:00', 1234987654, 'huongbenguyen@gmail.com', 'Tiền Giang', 2);

-- --------------------------------------------------------

--
-- Table structure for table `suckhoe`
--

CREATE TABLE `suckhoe` (
  `id` int(11) NOT NULL,
  `Ngaykiemtra` datetime NOT NULL,
  `Mahocsinh` int(11) NOT NULL,
  `Chieucao` int(11) NOT NULL,
  `Cannang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `suckhoe`
--

INSERT INTO `suckhoe` (`id`, `Ngaykiemtra`, `Mahocsinh`, `Chieucao`, `Cannang`) VALUES
(1, '2018-05-03 00:00:00', 1, 110, 19),
(2, '2018-05-03 00:00:00', 2, 95, 21),
(3, '2018-05-03 00:00:00', 3, 12, 85);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Malop` (`Malop`);

--
-- Indexes for table `baidang`
--
ALTER TABLE `baidang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Malop` (`Malop`,`Manguoidung`),
  ADD KEY `Manguoidung` (`Manguoidung`);

--
-- Indexes for table `chuansuckhoe`
--
ALTER TABLE `chuansuckhoe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diemdanh`
--
ALTER TABLE `diemdanh`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Mahocsinh` (`Mahocsinh`,`Malop`),
  ADD KEY `Malop` (`Malop`);

--
-- Indexes for table `hinhanh`
--
ALTER TABLE `hinhanh`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hocsinh`
--
ALTER TABLE `hocsinh`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Maphuhuynh` (`Maphuhuynh`),
  ADD KEY `Malop` (`Malop`),
  ADD KEY `Maphuhuynh_2` (`Maphuhuynh`,`Malop`);

--
-- Indexes for table `lop`
--
ALTER TABLE `lop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Malop` (`Malop`);

--
-- Indexes for table `nguoidung`
--
ALTER TABLE `nguoidung`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suckhoe`
--
ALTER TABLE `suckhoe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Mahocsinh` (`Mahocsinh`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `baidang`
--
ALTER TABLE `baidang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `chuansuckhoe`
--
ALTER TABLE `chuansuckhoe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `diemdanh`
--
ALTER TABLE `diemdanh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hinhanh`
--
ALTER TABLE `hinhanh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hocsinh`
--
ALTER TABLE `hocsinh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lop`
--
ALTER TABLE `lop`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `nguoidung`
--
ALTER TABLE `nguoidung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `suckhoe`
--
ALTER TABLE `suckhoe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `album`
--
ALTER TABLE `album`
  ADD CONSTRAINT `album_ibfk_1` FOREIGN KEY (`id`) REFERENCES `nguoidung` (`id`),
  ADD CONSTRAINT `album_ibfk_2` FOREIGN KEY (`Malop`) REFERENCES `lop` (`id`);

--
-- Constraints for table `baidang`
--
ALTER TABLE `baidang`
  ADD CONSTRAINT `baidang_ibfk_1` FOREIGN KEY (`Malop`) REFERENCES `lop` (`id`),
  ADD CONSTRAINT `baidang_ibfk_2` FOREIGN KEY (`Manguoidung`) REFERENCES `nguoidung` (`id`);

--
-- Constraints for table `diemdanh`
--
ALTER TABLE `diemdanh`
  ADD CONSTRAINT `diemdanh_ibfk_2` FOREIGN KEY (`Mahocsinh`) REFERENCES `hocsinh` (`id`),
  ADD CONSTRAINT `diemdanh_ibfk_3` FOREIGN KEY (`Malop`) REFERENCES `lop` (`id`);

--
-- Constraints for table `hinhanh`
--
ALTER TABLE `hinhanh`
  ADD CONSTRAINT `hinhanh_ibfk_1` FOREIGN KEY (`id`) REFERENCES `baidang` (`id`);

--
-- Constraints for table `hocsinh`
--
ALTER TABLE `hocsinh`
  ADD CONSTRAINT `hocsinh_ibfk_1` FOREIGN KEY (`Malop`) REFERENCES `lop` (`id`),
  ADD CONSTRAINT `hocsinh_ibfk_2` FOREIGN KEY (`Maphuhuynh`) REFERENCES `nguoidung` (`id`);

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`Malop`) REFERENCES `lop` (`id`);

--
-- Constraints for table `suckhoe`
--
ALTER TABLE `suckhoe`
  ADD CONSTRAINT `suckhoe_ibfk_2` FOREIGN KEY (`Mahocsinh`) REFERENCES `hocsinh` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
