<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('CHOpenAPI');
	}

	public function index()
	{
		$openAPI = new CHOpenAPI();

		var_dump($openAPI->healthPredict("0", "2017-01-01", 10.7, 80));
	}
}
