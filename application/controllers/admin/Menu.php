<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('date');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		session_start();

		if(!isset($_SESSION['user']))
		{
			$noti = array(
				'icon' => 'glyphicon glyphicon-exclamation-sign',
				'title' => "",
				'message' => "Bạn phải truy cập mới vào hệ thống!",
				'url' => "javascript:void(0)",
				'type' => 'danger'
			);
			$_SESSION["THONGBAO"]=$noti;
			redirect('/login');			
		}	
	}
	
	public function index(){
		$_SESSION['navi'] = 'qlthucdon';

		$this->load->model('Menu_m');
		$data['list']=$this->Menu_m->load_all_Menu();
		// print_r($data['list']);return;
		$this->load->view('admin/a_menu',$data);
	}
	public function add_menu()
	{
		$_SESSION['navi'] = 'qlthucdon';

		$this->load->view('admin/a_menu_add');
	}
	public function form_add_menu(){
		$Thoigianan = $this->input->post('Thoigianan');
		$Malop = $this->input->post('Malop');
		$BuoiSang = $this->input->post('BuoiSang');
		$BuoiTrua = $this->input->post('BuoiTrua');
		$BuoiToi = $this->input->post('BuoiToi');
		
		$this->load->model('Menu_m');
		$this->Menu_m->add_menu($Thoigianan, $Malop, $BuoiSang, $BuoiTrua, $BuoiToi);
		redirect('/admin/menu/');
	}
	public function edit_menu($id)
	{
		$_SESSION['navi'] = 'qlthucdon';
		$this->load->model('Lop_m');
		$data['listLop'] = $this->Lop_m->load_all_lop();
		$this->load->model('Menu_m');
		$data['list']=$this->Menu_m->get_menu_by_id($id);
		$this->load->view('admin/a_menu_edit',$data);
	}
	public function form_edit_menu(){
		$id = $this->input->post('id');
		$Thoigianan = $this->input->post('Thoigianan');
		$Malop = $this->input->post('Malop');
		$BuoiSang = $this->input->post('BuoiSang');
		$BuoiTrua = $this->input->post('BuoiTrua');
		$BuoiToi = $this->input->post('BuoiToi');
		
		$this->load->model('Menu_m');
		$this->Menu_m->edit_menu_by_id($id, $Thoigianan, $Malop, $BuoiSang, $BuoiTrua, $BuoiToi);
		redirect('/admin/menu/');
	}
	public function delete_menu($id)
	{
		$this->load->model('Menu_m');
		$data['list']=$this->Menu_m->delete_menu_by_id($id);
		redirect('/admin/menu/');
	
	}
}