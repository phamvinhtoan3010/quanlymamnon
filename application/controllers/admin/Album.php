<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Album extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('date');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		session_start();
		
		if(!isset($_SESSION['user']))
		{
			$noti = array(
				'icon' => 'glyphicon glyphicon-exclamation-sign',
				'title' => "",
				'message' => "Bạn phải truy cập mới vào hệ thống!",
				'url' => "javascript:void(0)",
				'type' => 'danger'
			);
			$_SESSION["THONGBAO"]=$noti;
			redirect('/login');			
		}	
	}
	
	public function index(){
		$_SESSION['navi'] = 'qlalbum';

		$this->load->model('Album_m');
		$data['list']=$this->Album_m->load_all_Album();
		// print_r($data['list']);return;
		$this->load->view('admin/a_album',$data);
	}
	public function add_album()
	{
		$_SESSION['navi'] = 'qlalbum';
		
		$this->load->view('admin/a_album_add');
	}
	public function form_add_album(){
		$Tenalbum = $this->input->post('Tenalbum');
		$Giodang = $this->input->post('Giodang');
		$Malop = $this->input->post('Malop');
		$Mota = $this->input->post('Mota');
		$Soluong = $this->input->post('Soluong');
		
		$this->load->model('Album_m');
		$this->Album_m->add_album($Tenalbum, $Giodang, $Malop, $Mota, $Soluong);
		redirect('/admin/album/');
	}
	public function edit_album($id)
	{
		$_SESSION['navi'] = 'qlalbum';
		
		$this->load->model('Album_m');
		$data['list']=$this->Album_m->get_album_by_id($id);
		$this->load->view('admin/a_album_edit',$data);
	}
	public function form_edit_album(){
		$id = $this->input->post('id');
		$Tenalbum = $this->input->post('Tenalbum');
		$Giodang = $this->input->post('Giodang');
		$Malop = $this->input->post('Malop');
		$Mota = $this->input->post('Mota');
		$Soluong = $this->input->post('Soluong');
		
		$this->load->model('Album_m');
		$this->Album_m->edit_album_by_id($id, $Tenalbum, $Giodang, $Malop, $Mota, $Soluong);
		redirect('/admin/album/');
	}
	public function delete_album($id)
	{
		$this->load->model('Album_m');
		$data['list']=$this->Album_m->delete_album_by_id($id);
		redirect('/admin/album/');
	
	}
}