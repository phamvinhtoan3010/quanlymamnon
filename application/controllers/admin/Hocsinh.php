<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hocsinh extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('date');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		session_start();

		if(!isset($_SESSION['user']))
		{
			$noti = array(
				'icon' => 'glyphicon glyphicon-exclamation-sign',
				'title' => "",
				'message' => "Bạn phải truy cập mới vào hệ thống!",
				'url' => "javascript:void(0)",
				'type' => 'danger'
			);
			$_SESSION["THONGBAO"]=$noti;
			redirect('/login');			
		}	
	}
	
	public function index(){
		$_SESSION['navi'] = 'qltre';

		$this->load->model('Hocsinh_m');
		$data['list']=$this->Hocsinh_m->load_all_Hocsinh();
		// print_r($data['list']);return;
		$this->load->view('admin/a_hocsinh',$data);
	}
	public function add_hocsinh()
	{
		$_SESSION['navi'] = 'qltre';

		$this->load->model('Nguoidung_m');
		$data['listPH']=$this->Nguoidung_m->load_all_nguoidung();
		// print_r($data);
		$this->load->model('Lop_m');
		$data['listLop']=$this->Lop_m->load_all_lop();
		//print_r($data);
		$this->load->view('admin/a_hocsinh_add', $data);
	}
	public function form_add_hocsinh(){
		$Hoten = $this->input->post('Hoten');
		$Nickname = $this->input->post('Nickname');
		$Gioitinh = $this->input->post('Gioitinh');
		$Ngaysinh = $this->input->post('Ngaysinh');
		$Maphuhuynh = $this->input->post('Maphuhuynh');
		$Malop = $this->input->post('Malop');
		
		$this->load->model('Hocsinh_m');
		$this->Hocsinh_m->add_hocsinh($Hoten, $Nickname, $Gioitinh, $Ngaysinh, $Maphuhuynh, $Malop);
		redirect('/admin/hocsinh/');
	}
	public function edit_hocsinh($id)
	{
		$_SESSION['navi'] = 'qltre';

		$this->load->model('Hocsinh_m');
		$this->load->model('Lop_m');
		$data['list']=$this->Hocsinh_m->get_hocsinh_by_id($id);
		$data['listLop']=$this->Lop_m->load_all_lop();
		$this->load->view('admin/a_hocsinh_edit',$data);
	}
	public function form_edit_hocsinh(){
		$id = $this->input->post('id');
		$Hoten = $this->input->post('Hoten');
		$Nickname = $this->input->post('Nickname');
		$Gioitinh = $this->input->post('Gioitinh');
		$Ngaysinh = $this->input->post('Ngaysinh');
		$Maphuhuynh = $this->input->post('Maphuhuynh');
		$Malop = $this->input->post('Malop');
		
		$this->load->model('Hocsinh_m');
		$this->Hocsinh_m->edit_hocsinh_by_id($id, $Hoten, $Nickname, $Gioitinh, $Ngaysinh, $Maphuhuynh, $Malop);
		redirect('/admin/hocsinh/');
	}
	public function delete_hocsinh($id)
	{
		$this->load->model('Hocsinh_m');
		$data['list']=$this->Hocsinh_m->delete_hocsinh_by_id($id);
		redirect('/admin/hocsinh/');
	
	}
}