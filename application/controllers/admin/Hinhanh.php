<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hinhanh extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		session_start();
		
		if(!isset($_SESSION['user']))
		{
			$noti = array(
				'icon' => 'glyphicon glyphicon-exclamation-sign',
				'title' => "",
				'message' => "Bạn phải truy cập mới vào hệ thống!",
				'url' => "javascript:void(0)",
				'type' => 'danger'
			);
			$_SESSION["THONGBAO"]=$noti;
			redirect('/login');			
		}	
	}
	
	public function index(){
		$this->load->model('Hinhanh_m');
		$data['list']=$this->Hinhanh_m->load_all_Hinhanh();
		// print_r($data['list']);return;
		$this->load->view('admin/a_hinhanh',$data);
	}
	public function add_hinhanh()
	{
		$this->load->view('admin/a_hinhanh_add');
	}
	public function form_add_hinhanh(){
		$filename = $this->input->post('filename');
		$type = $this->input->post('type');
		
		$this->load->model('Hinhanh_m');
		$this->Hinhanh_m->add_hinhanh($filename, $type);
		redirect('/admin/hinhanh/');
	}
	public function edit_hinhanh($id)
	{
		$this->load->model('Hinhanh_m');
		$data['list']=$this->Hinhanh_m->get_hinhanh_by_id($id);
		$this->load->view('admin/a_hinhanh_edit',$data);
	}
	public function form_edit_hinhanh(){
		$id = $this->input->post('id');
		$filename = $this->input->post('filename');
		$type = $this->input->post('type');
		
		$this->load->model('Hinhanh_m');
		$this->Hinhanh_m->edit_hinhanh_by_id($id, $filename, $type);
		redirect('/admin/hinhanh/');
	}
	public function delete_hinhanh($id)
	{
		$this->load->model('Hinhanh_m');
		$data['list']=$this->Hinhanh_m->delete_hinhanh_by_id($id);
		redirect('/admin/hinhanh/');
	
	}
}