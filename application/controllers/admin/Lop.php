<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lop extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		session_start();

		if(!isset($_SESSION['user']))
		{
			$noti = array(
				'icon' => 'glyphicon glyphicon-exclamation-sign',
				'title' => "",
				'message' => "Bạn phải truy cập mới vào hệ thống!",
				'url' => "javascript:void(0)",
				'type' => 'danger'
			);
			$_SESSION["THONGBAO"]=$noti;
			redirect('/login');			
		}	
	}
	
	public function index(){
		$_SESSION['navi'] = 'qllop';
		
		$this->load->model('Lop_m');
		$data['list']=$this->Lop_m->load_all_Lop();
		// print_r($data['list']);return;
		$this->load->view('admin/a_lop',$data);
	}
	public function add_lop()
	{
		$_SESSION['navi'] = 'qllop';

		$this->load->model('Lop_m');
		$data['listLop']=$this->Lop_m->load_all_Lop();
		// print_r($data);
		$this->load->view('admin/a_lop_add', $data);
	}
	public function form_add_lop(){
		$Tenlop = $this->input->post('Tenlop');
		$Giaovien1 = $this->input->post('Giaovien1');
		$Giaovien2 = $this->input->post('Giaovien2');
		$Giaovien3 = $this->input->post('Giaovien3');
		
		$this->load->model('Lop_m');
		$this->Lop_m->add_lop($Tenlop, $Giaovien1, $Giaovien2, $Giaovien3);
		redirect('/admin/lop/');
	}
	public function edit_lop($id)
	{
		$_SESSION['navi'] = 'qllop';

		$this->load->model('Lop_m');
		$data['list']=$this->Lop_m->get_lop_by_id($id);
		$this->load->view('admin/a_lop_edit',$data);
	}
	public function form_edit_lop(){
		$id = $this->input->post('id');
		$Tenlop = $this->input->post('Tenlop');
		$Giaovien1 = $this->input->post('Giaovien1');
		$Giaovien2 = $this->input->post('Giaovien2');
		$Giaovien3 = $this->input->post('Giaovien3');
		
		$this->load->model('Lop_m');
		$this->Lop_m->edit_lop_by_id($id, $Tenlop, $Giaovien1, $Giaovien2, $Giaovien3);
		redirect('/admin/lop/');
	}
	public function delete_lop($id)
	{
		$this->load->model('Lop_m');
		$data['list']=$this->Lop_m->delete_lop_by_id($id);
		redirect('/admin/lop/');
	
	}
}