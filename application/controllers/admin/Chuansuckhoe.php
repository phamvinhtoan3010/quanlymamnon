<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chuansuckhoe extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		session_start();
		
		if(!isset($_SESSION['user']))
		{
			$noti = array(
				'icon' => 'glyphicon glyphicon-exclamation-sign',
				'title' => "",
				'message' => "Bạn phải truy cập mới vào hệ thống!",
				'url' => "javascript:void(0)",
				'type' => 'danger'
			);
			$_SESSION["THONGBAO"]=$noti;
			redirect('/login');			
		}	
	}
	
	public function index(){
		$this->load->model('Chuansuckhoe_m');
		$data['list']=$this->Chuansuckhoe_m->load_all_Chuansuckhoe();
		// print_r($data['list']);return;
		$this->load->view('admin/a_chuansuckhoe',$data);
	}
	public function add_chuansuckhoe()
	{
		$this->load->view('admin/a_chuansuckhoe_add');
	}
	public function form_add_chuansuckhoe(){
		$Gioitinh = $this->input->post('Gioitinh');
		$Thang = $this->input->post('Thang');
		$Tuoi = $this->input->post('Tuoi');
		$w1 = $this->input->post('w1');
		$h1 = $this->input->post('h1');
		$w2 = $this->input->post('w2');
		$h2 = $this->input->post('h2');
		$w3 = $this->input->post('w3');
		$h3 = $this->input->post('h3');
		
		$this->load->model('Chuansuckhoe_m');
		$this->Chuansuckhoe_m->add_chuansuckhoe($Gioitinh, $Thang, $Tuoi, $w1, $h1, $w2, $h2, $w3, $h3);
		redirect('/admin/chuansuckhoe/');
	}
	public function edit_chuansuckhoe($id)
	{
		$this->load->model('Chuansuckhoe_m');
		$data['list']=$this->Chuansuckhoe_m->get_chuansuckhoe_by_id($id);
		$this->load->view('admin/a_chuansuckhoe_edit',$data);
	}
	public function form_edit_chuansuckhoe(){
		$id = $this->input->post('id');
		$Gioitinh = $this->input->post('Gioitinh');
		$Thang = $this->input->post('Thang');
		$Tuoi = $this->input->post('Tuoi');
		$w1 = $this->input->post('w1');
		$h1 = $this->input->post('h1');
		$w2 = $this->input->post('w2');
		$h2 = $this->input->post('h2');
		$w3 = $this->input->post('w3');
		$h3 = $this->input->post('h3');
		
		$this->load->model('Chuansuckhoe_m');
		$this->Chuansuckhoe_m->edit_chuansuckhoe_by_id($id, $Gioitinh, $Thang, $Tuoi, $w1, $h1, $w2, $h2, $w3, $h3);
		redirect('/admin/chuansuckhoe/');
	}
	public function delete_chuansuckhoe($id)
	{
		$this->load->model('Chuansuckhoe_m');
		$data['list']=$this->Chuansuckhoe_m->delete_chuansuckhoe_by_id($id);
		redirect('/admin/chuansuckhoe/');
	
	}
}