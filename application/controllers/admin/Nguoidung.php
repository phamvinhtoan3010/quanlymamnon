<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nguoidung extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('date');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		session_start();

		if(!isset($_SESSION['user']))
		{
			$noti = array(
				'icon' => 'glyphicon glyphicon-exclamation-sign',
				'title' => "",
				'message' => "Bạn phải truy cập mới vào hệ thống!",
				'url' => "javascript:void(0)",
				'type' => 'danger'
			);
			$_SESSION["THONGBAO"]=$noti;
			redirect('/login');			
		}	
	}
	
	public function index(){
		$_SESSION['navi'] = 'qltaikhoan';
		$this->load->model('Nguoidung_m');
		$data['list']=$this->Nguoidung_m->load_all_Nguoidung();
		// print_r($data['list']);return;
		$this->load->view('admin/a_nguoidung',$data);
	}
	public function add_nguoidung()
	{
		$_SESSION['navi'] = 'qltaikhoan';
		$this->load->view('admin/a_nguoidung_add');
	}
	public function form_add_nguoidung(){
		$tendangnhap = $this->input->post('tendangnhap');
		$Matkhau = $this->input->post('Matkhau');
		$Hoten = $this->input->post('Hoten');
		$Gioitinh = $this->input->post('Gioitinh');
		$Ngaysinh = $this->input->post('Ngaysinh');
		$SDT = $this->input->post('SDT');
		$Email = $this->input->post('Email');
		$Diachi = $this->input->post('Diachi');
		$Quyentruycap = $this->input->post('Quyentruycap');
		
		$this->load->model('Nguoidung_m');
		$this->Nguoidung_m->add_nguoidung($tendangnhap, $Matkhau, $Hoten, $Gioitinh, $Ngaysinh, $SDT, $Email, $Diachi, $Quyentruycap);
		redirect('/admin/nguoidung/');
	}
	public function edit_nguoidung($id)
	{
		$_SESSION['navi'] = 'qltaikhoan';

		$this->load->model('Nguoidung_m');
		$data['list']=$this->Nguoidung_m->get_nguoidung_by_id($id);
		$this->load->view('admin/a_nguoidung_edit',$data);
	}
	public function form_edit_nguoidung(){
		$id = $this->input->post('id');
		$tendangnhap = $this->input->post('tendangnhap');
		$Matkhau = $this->input->post('Matkhau');
		$Hoten = $this->input->post('Hoten');
		$Gioitinh = $this->input->post('Gioitinh');
		$Ngaysinh = $this->input->post('Ngaysinh');
		$SDT = $this->input->post('SDT');
		$Email = $this->input->post('Email');
		$Diachi = $this->input->post('Diachi');
		$Quyentruycap = $this->input->post('Quyentruycap');
		
		$this->load->model('Nguoidung_m');
		$this->Nguoidung_m->edit_nguoidung_by_id($id, $tendangnhap, $Matkhau, $Hoten, $Gioitinh, $Ngaysinh, $SDT, $Email, $Diachi, $Quyentruycap);
		redirect('/admin/nguoidung/');
	}
	public function delete_nguoidung($id)
	{
		$this->load->model('Nguoidung_m');
		$data['list']=$this->Nguoidung_m->delete_nguoidung_by_id($id);
		redirect('/admin/nguoidung/');
	
	}
}