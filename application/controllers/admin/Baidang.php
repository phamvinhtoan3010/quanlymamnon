<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Baidang extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('date');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		session_start();
		
		if(!isset($_SESSION['user']))
		{
			$noti = array(
				'icon' => 'glyphicon glyphicon-exclamation-sign',
				'title' => "",
				'message' => "Bạn phải truy cập mới vào hệ thống!",
				'url' => "javascript:void(0)",
				'type' => 'danger'
			);
			$_SESSION["THONGBAO"]=$noti;
			redirect('/login');			
		}	
	}
	
	public function index(){
		$_SESSION['navi'] = 'qlbaiviet';

		$this->load->model('Baidang_m');
		$data['list']=$this->Baidang_m->load_all_Baidang();
		// print_r($data['list']);return;
		$this->load->view('admin/a_baidang',$data);
	}
	public function add_baidang()
	{
		$_SESSION['navi'] = 'qlbaiviet';

		$this->load->view('admin/a_baidang_add');
	}
	public function form_add_baidang(){
		$Malop = $this->input->post('Malop');
		$Manguoidung = $this->input->post('Manguoidung');
		$Noidung = $this->input->post('Noidung');
		$Ngaydang = $this->input->post('Ngaydang');
		
		$this->load->model('Baidang_m');
		$this->Baidang_m->add_baidang($Malop, $Manguoidung, $Noidung, $Ngaydang);
		redirect('/admin/baidang/');
	}
	public function edit_baidang($id)
	{
		$_SESSION['navi'] = 'qlbaiviet';
		
		$this->load->model('Baidang_m');
		$data['list']=$this->Baidang_m->get_baidang_by_id($id);
		$this->load->view('admin/a_baidang_edit',$data);
	}
	public function form_edit_baidang(){
		$id = $this->input->post('id');
		$Malop = $this->input->post('Malop');
		$Manguoidung = $this->input->post('Manguoidung');
		$Noidung = $this->input->post('Noidung');
		$Ngaydang = $this->input->post('Ngaydang');
		
		$this->load->model('Baidang_m');
		$this->Baidang_m->edit_baidang_by_id($id, $Malop, $Manguoidung, $Noidung, $Ngaydang);
		redirect('/admin/baidang/');
	}
	public function delete_baidang($id)
	{
		$this->load->model('Baidang_m');
		$data['list']=$this->Baidang_m->delete_baidang_by_id($id);
		redirect('/admin/baidang/');
	
	}
}