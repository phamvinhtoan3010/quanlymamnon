<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suckhoe extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		session_start();

		if(!isset($_SESSION['user']))
		{
			$noti = array(
				'icon' => 'glyphicon glyphicon-exclamation-sign',
				'title' => "",
				'message' => "Bạn phải truy cập mới vào hệ thống!",
				'url' => "javascript:void(0)",
				'type' => 'danger'
			);
			$_SESSION["THONGBAO"]=$noti;
			redirect('/login');			
		}	
	}
	
	public function index(){
		$_SESSION['navi'] = 'qlsuckhoe';

		$this->load->model('Suckhoe_m');
		$data['list']=$this->Suckhoe_m->load_all_Suckhoe();
		// print_r($data['list']);return;
		$this->load->view('admin/a_suckhoe',$data);
	}
	public function add_suckhoe()
	{
		$_SESSION['navi'] = 'qlsuckhoe';

		$this->load->view('admin/a_suckhoe_add');
	}
	public function form_add_suckhoe(){
		$Ngaykiemtra = $this->input->post('Ngaykiemtra');
		$Mahocsinh = $this->input->post('Mahocsinh');
		$Chieucao = $this->input->post('Chieucao');
		$Cannang = $this->input->post('Cannang');
		
		$this->load->model('Suckhoe_m');
		$this->Suckhoe_m->add_suckhoe($Ngaykiemtra, $Mahocsinh, $Chieucao, $Cannang);
		redirect('/admin/suckhoe/');
	}
	public function edit_suckhoe($id)
	{
		$_SESSION['navi'] = 'qlsuckhoe';
		
		$this->load->model('Suckhoe_m');
		$data['list']=$this->Suckhoe_m->get_suckhoe_by_id($id);
		$this->load->view('admin/a_suckhoe_edit',$data);
	}
	public function form_edit_suckhoe(){
		$id = $this->input->post('id');
		$Ngaykiemtra = $this->input->post('Ngaykiemtra');
		$Mahocsinh = $this->input->post('Mahocsinh');
		$Chieucao = $this->input->post('Chieucao');
		$Cannang = $this->input->post('Cannang');
		
		$this->load->model('Suckhoe_m');
		$this->Suckhoe_m->edit_suckhoe_by_id($id, $Ngaykiemtra, $Mahocsinh, $Chieucao, $Cannang);
		redirect('/admin/suckhoe/');
	}
	public function delete_suckhoe($id)
	{
		$this->load->model('Suckhoe_m');
		$data['list']=$this->Suckhoe_m->delete_suckhoe_by_id($id);
		redirect('/admin/suckhoe/');
	
	}
}