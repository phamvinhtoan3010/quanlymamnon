<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diemdanh extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('date');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		session_start();
		
		if(!isset($_SESSION['user']))
		{
			$noti = array(
				'icon' => 'glyphicon glyphicon-exclamation-sign',
				'title' => "",
				'message' => "Bạn phải truy cập mới vào hệ thống!",
				'url' => "javascript:void(0)",
				'type' => 'danger'
			);
			$_SESSION["THONGBAO"]=$noti;
			redirect('/login');			
		}	
	}
	
	public function index(){
		$_SESSION['navi'] = 'qldiemdanh';

		$this->load->model('Diemdanh_m');
		$data['list']=$this->Diemdanh_m->load_all_Diemdanh();
		// print_r($data['list']);return;
		$this->load->view('admin/a_diemdanh',$data);
	}
	public function add_diemdanh()
	{
		$_SESSION['navi'] = 'qldiemdanh';

		$this->load->view('admin/a_diemdanh_add');
	}
	public function form_add_diemdanh(){
		$Mahocsinh = $this->input->post('Mahocsinh');
		$Malop = $this->input->post('Malop');
		$Giodiemdanh = $this->input->post('Giodiemdanh');
		$Trangthai = $this->input->post('Trangthai');
		
		$this->load->model('Diemdanh_m');
		$this->Diemdanh_m->add_diemdanh($Mahocsinh, $Malop, $Giodiemdanh, $Trangthai);
		redirect('/admin/diemdanh/');
	}
	public function edit_diemdanh($id)
	{
		$_SESSION['navi'] = 'qldiemdanh';
		
		$this->load->model('Diemdanh_m');
		$data['list']=$this->Diemdanh_m->get_diemdanh_by_id($id);
		$this->load->view('admin/a_diemdanh_edit',$data);
	}
	public function form_edit_diemdanh(){
		$id = $this->input->post('id');
		$Mahocsinh = $this->input->post('Mahocsinh');
		$Malop = $this->input->post('Malop');
		$Giodiemdanh = $this->input->post('Giodiemdanh');
		$Trangthai = $this->input->post('Trangthai');
		
		$this->load->model('Diemdanh_m');
		$this->Diemdanh_m->edit_diemdanh_by_id($id, $Mahocsinh, $Malop, $Giodiemdanh, $Trangthai);
		redirect('/admin/diemdanh/');
	}
	public function delete_diemdanh($id)
	{
		$this->load->model('Diemdanh_m');
		$data['list']=$this->Diemdanh_m->delete_diemdanh_by_id($id);
		redirect('/admin/diemdanh/');
	
	}
}