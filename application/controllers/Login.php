<?php
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        session_start();
        $this->load->helper('form');
        $this->load->helper('url');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->load->model("Login_m");
        // $this->load->model("User_m");     
	}
    public function index()
    {
        $this->load->view('login'); 
    }
    public function login_user()
    {
        $username = $this->input->post('ten_dang_nhap');
        $pass     = ($this->input->post('mat_khau'));
        if($username !=null && $pass != null)
        {
            if ($this->Login_m->check_login($username, $pass) == true ) {
            
                if($_SESSION['user']->Quyentruycap == "1")
                {
                    redirect('/home');
                }
                else{
                    $noti = array(
                        'icon' => 'glyphicon glyphicon-exclamation-sign',
                        'title' => "",
                        'message' => "Bạn không có quyền truy cập vào hệ thống!",
                        'url' => "javascript:void(0)",
                        'type' => 'danger'
                    );
                    $_SESSION["THONGBAO"]=$noti;
                    redirect('/login');
                    // $this->load->view('login');
                }
            }
            else{
                $noti = array(
                    'icon' => 'glyphicon glyphicon-exclamation-sign',
                    'title' => "",
                    'message' => "Sai tài khoản hoặc mật khẩu!",
                    'url' => "javascript:void(0)",
                    'type' => 'danger'
                );
                $_SESSION["THONGBAO"]=$noti;
                redirect('/login');
                    // $this->load->view('login');
            }
        }
        else{
            $noti = array(
                'icon' => 'glyphicon glyphicon-exclamation-sign',
                'title' => "",
                'message' => "Sai tài khoản hoặc mật khẩu!",
                'url' => "javascript:void(0)",
                'type' => 'danger'
            );
            $_SESSION["THONGBAO"]=$noti;
            redirect('/login');
            
        }
        
    }
    public function recover()
    {
        $noti = array(
            'icon' => 'glyphicon glyphicon-share-alt',
            'title' => "",
            'message' => "Liên hệ với admin để lấy lại mật khẩu!",
            'url' => "javascript:void(0)",
            'type' => 'info'
        );
        $_SESSION["THONGBAO"]=$noti;
        redirect('/login');
        
        
    }

    public function logout()
    {
        session_destroy();
        redirect('/login');

    }
}
