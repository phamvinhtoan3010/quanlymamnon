<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->library('CHOpenAPI');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		session_start();

		if(isset($_SESSION['user']))
		{
			$noti = array(
				'icon' => 'glyphicon glyphicon-sign-thumbs-up',
				'title' => "",
				'message' => "Xin chào quản trị viên ".$_SESSION['user']->Hoten,
				'url' => "javascript:void(0)",
				'type' => 'success'
			);
			$_SESSION["THONGBAO"]=$noti;
				
		}
		else{
			$noti = array(
				'icon' => 'glyphicon glyphicon-exclamation',
				'title' => "",
				'message' => "Bạn phải truy cập mới vào hệ thống!",
				'url' => "javascript:void(0)",
				'type' => 'danger'
			);
			$_SESSION["THONGBAO"]=$noti;
			redirect('/login');		
		}
	}

	public function index()
	{
		$openAPI = new CHOpenAPI();
		$_SESSION['navi'] = 'tongquan';

		// Thong ke nhanh
		$this->load->model('Nguoidung_m');
		$this->load->model('Hocsinh_m');
		$this->load->model('Baidang_m');
		$this->load->model('Album_m');
		$data['nNguoidung'] = count($this->Nguoidung_m->load_all_nguoidung());
		$data['nHocsinh'] = count($this->Hocsinh_m->load_all_hocsinh());
		$data['nBaidang'] = count($this->Baidang_m->load_all_baidang());
		$data['nAlbum'] = count($this->Album_m->load_all_album());

		// Load suc khoe
		$this->load->model('Suckhoe_m');
		$healthList = $this->Suckhoe_m->get_suckhoe_with_hocsinh_info();

		
		$data['suckhoe'] = array();
		foreach ($healthList as $item) {
			$predict = $openAPI->healthPredict($item->Gioitinh, mdate('%Y-%m-%d', mysql_to_unix($item->Ngaysinh)), $item->Cannang, $item->Chieucao);
			
			// W Predict
			switch ($predict->predict1) {
				case "Suy dinh dưỡng":
					$bg1 = "bg-red";
				break;
				case "Có nguy cơ suy dinh dưỡng":
					$bg1 = "bg-orange";
				break;	
				case "Đạt chuẩn":
					$bg1 = "bg-green";
				break;
				case "Béo phì":
					$bg1 = "bg-light-blue";
				break;
			}

			// H Predict
			switch ($predict->predict2) {
				case "Quá thấp":
					$bg2 = "bg-red";
				break;
				case "Thiếu chiều cao":
					$bg2 = "bg-orange";
				break;
				case "Đạt chuẩn":
					$bg2 = "bg-green";	
				break;
				case "Chiều cao phát triển nhanh":
					$bg2 = "bg-light-blue";
				break;
			}

			array_push($data['suckhoe'], array(
					"fullname" => $item->Hoten,
					"nickname" => $item->Nickname,
					"gender" => $item->Gioitinh,
					"height" => $item->Chieucao,
					"weight" => $item->Cannang,
					"predict1" => $predict->predict1,
					"predict2" => $predict->predict2,
					"bg1" => $bg1,
					"bg2" => $bg2,
				)
			);
		}

		$this->load->view('admin/index', $data);
	}
}
