<?php
class Hinhanh_m extends CI_Model
{
	
	function load_all_hinhanh()
	{
		$this->load->database();
		$this->db->from('hinhanh');
		$list = $this->db->get()->result();
		return $list;
	}
	
	
	function get_hinhanh_by_id($id)
	{
		$this->load->database();
		$this->db->from('hinhanh');
		$this->db->where('id',$id);
		$list = $this->db->get()->result()[0];
		return $list;
	}
	
	
	function add_hinhanh($filename, $type){
		
		$this->load->database();
		$this->db->from('hinhanh');
		$data = array(
				'filename' => $filename, 
				'type' => $type);
		return $this->db->insert('hinhanh',$data);
	}
	
	function edit_hinhanh_by_id($id, $filename, $type){
		$this->load->database();
		$this->db->from('hinhanh');
		$this->db->where('id',$id);
		$data = array(
				'filename' => $filename, 
				'type' => $type);
		return $this->db->update('hinhanh',$data);
	} 
	
	public function delete_hinhanh_by_id($id){
		$this->load->database();
		$this->db->from('hinhanh');
		$this->db->where('id',$id);
		return $this->db->delete('hinhanh');
	}}