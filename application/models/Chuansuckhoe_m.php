<?php
class Chuansuckhoe_m extends CI_Model
{
	
	function load_all_chuansuckhoe()
	{
		$this->load->database();
		$this->db->from('chuansuckhoe');
		$list = $this->db->get()->result();
		return $list;
	}
	
	
	function get_chuansuckhoe_by_id($id)
	{
		$this->load->database();
		$this->db->from('chuansuckhoe');
		$this->db->where('id',$id);
		$list = $this->db->get()->result()[0];
		return $list;
	}
	
	
	function add_chuansuckhoe($Gioitinh, $Thang, $Tuoi, $w1, $h1, $w2, $h2, $w3, $h3){
		
		$this->load->database();
		$this->db->from('chuansuckhoe');
		$data = array(
				'Gioitinh' => $Gioitinh, 
				'Thang' => $Thang, 
				'Tuoi' => $Tuoi, 
				'w1' => $w1, 
				'h1' => $h1, 
				'w2' => $w2, 
				'h2' => $h2, 
				'w3' => $w3, 
				'h3' => $h3);
		return $this->db->insert('chuansuckhoe',$data);
	}
	
	function edit_chuansuckhoe_by_id($id, $Gioitinh, $Thang, $Tuoi, $w1, $h1, $w2, $h2, $w3, $h3){
		$this->load->database();
		$this->db->from('chuansuckhoe');
		$this->db->where('id',$id);
		$data = array(
				'Gioitinh' => $Gioitinh, 
				'Thang' => $Thang, 
				'Tuoi' => $Tuoi, 
				'w1' => $w1, 
				'h1' => $h1, 
				'w2' => $w2, 
				'h2' => $h2, 
				'w3' => $w3, 
				'h3' => $h3);
		return $this->db->update('chuansuckhoe',$data);
	} 
	
	public function delete_chuansuckhoe_by_id($id){
		$this->load->database();
		$this->db->from('chuansuckhoe');
		$this->db->where('id',$id);
		return $this->db->delete('chuansuckhoe');
	}}