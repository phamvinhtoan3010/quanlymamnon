<?php
class Baidang_m extends CI_Model
{
	
	function load_all_baidang()
	{
		$this->load->database();
		$this->db->select('b.*,l.Tenlop as tenlop,n.Hoten as tennd');			
		$this->db->from('baidang b');
		$this->db->join('nguoidung n ', 'n.id = b.Manguoidung');
		$this->db->join('lop l ', 'l.id = b.Malop');		
		$list = $this->db->get()->result();
		return $list;
	}
	
	
	function get_baidang_by_id($id)
	{
		$this->load->database();
		$this->db->from('baidang');
		$this->db->where('id',$id);
		$list = $this->db->get()->result()[0];
		return $list;
	}
	
	
	function add_baidang($Malop, $Manguoidung, $Noidung, $Ngaydang){
		
		$this->load->database();
		$this->db->from('baidang');
		$data = array(
				'Malop' => $Malop, 
				'Manguoidung' => $Manguoidung, 
				'Noidung' => $Noidung, 
				'Ngaydang' => $Ngaydang);
		return $this->db->insert('baidang',$data);
	}
	
	function edit_baidang_by_id($id, $Malop, $Manguoidung, $Noidung, $Ngaydang){
		$this->load->database();
		$this->db->from('baidang');
		$this->db->where('id',$id);
		$data = array(
				'Malop' => $Malop, 
				'Manguoidung' => $Manguoidung, 
				'Noidung' => $Noidung, 
				'Ngaydang' => $Ngaydang);
		return $this->db->update('baidang',$data);
	} 
	
	public function delete_baidang_by_id($id){
		$this->load->database();
		$this->db->from('baidang');
		$this->db->where('id',$id);
		return $this->db->delete('baidang');
	}}