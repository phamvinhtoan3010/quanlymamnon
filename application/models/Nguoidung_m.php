<?php
class Nguoidung_m extends CI_Model
{
	
	function load_all_nguoidung()
	{
		$this->load->database();
		$this->db->from('nguoidung');
		$list = $this->db->get()->result();
		return $list;
	}
	
	
	function get_nguoidung_by_id($id)
	{
		$this->load->database();
		$this->db->from('nguoidung');
		$this->db->where('id',$id);
		$list = $this->db->get()->result()[0];
		return $list;
	}
	
	
	function add_nguoidung($tendangnhap, $Matkhau, $Hoten, $Gioitinh, $Ngaysinh, $SDT, $Email, $Diachi, $Quyentruycap){
		
		$this->load->database();
		$this->db->from('nguoidung');
		$data = array(
				'tendangnhap' => $tendangnhap, 
				'Matkhau' => md5($Matkhau), 
				'Hoten' => $Hoten, 
				'Gioitinh' => $Gioitinh, 
				'Ngaysinh' => $Ngaysinh, 
				'SDT' => $SDT, 
				'Email' => $Email, 
				'Diachi' => $Diachi, 
				'Quyentruycap' => $Quyentruycap);
		return $this->db->insert('nguoidung',$data);
	}
	
	function edit_nguoidung_by_id($id, $tendangnhap, $Matkhau, $Hoten, $Gioitinh, $Ngaysinh, $SDT, $Email, $Diachi, $Quyentruycap){
		$this->load->database();
		$this->db->from('nguoidung');
		$this->db->where('id',$id);
		$data = array(
				'tendangnhap' => $tendangnhap, 
				'Matkhau' => md5($Matkhau), 
				'Hoten' => $Hoten, 
				'Gioitinh' => $Gioitinh, 
				'Ngaysinh' => $Ngaysinh, 
				'SDT' => $SDT, 
				'Email' => $Email, 
				'Diachi' => $Diachi, 
				'Quyentruycap' => $Quyentruycap);
		return $this->db->update('nguoidung',$data);
	} 
	
	public function delete_nguoidung_by_id($id){
		$this->load->database();
		$this->db->from('nguoidung');
		$this->db->where('id',$id);
		return $this->db->delete('nguoidung');
	}}