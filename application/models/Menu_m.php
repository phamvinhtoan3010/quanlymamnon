<?php
class Menu_m extends CI_Model
{
	
	function load_all_menu()
	{
		$this->load->database();
		$this->db->select('m.*, l.Tenlop as tenlop');			
		$this->db->from('menu m');
		$this->db->join('lop l ', 'l.id = m.Malop');
		$list = $this->db->get()->result();
		return $list;
	}
	
	
	function get_menu_by_id($id)
	{
		$this->load->database();
		$this->db->from('menu');
		$this->db->where('id',$id);
		$list = $this->db->get()->result()[0];
		return $list;
	}
	
	
	function add_menu($Thoigianan, $Malop, $BuoiSang, $BuoiTrua, $BuoiToi){
		
		$this->load->database();
		$this->db->from('menu');
		$data = array(
				'Thoigianan' => $Thoigianan, 
				'Malop' => $Malop, 
				'BuoiSang' => $BuoiSang, 
				'BuoiTrua' => $BuoiTrua, 
				'BuoiToi' => $BuoiToi);
		return $this->db->insert('menu',$data);
	}
	
	function edit_menu_by_id($id, $Thoigianan, $Malop, $BuoiSang, $BuoiTrua, $BuoiToi){
		$this->load->database();
		$this->db->from('menu');
		$this->db->where('id',$id);
		$data = array(
				'Thoigianan' => $Thoigianan, 
				'Malop' => $Malop, 
				'BuoiSang' => $BuoiSang, 
				'BuoiTrua' => $BuoiTrua, 
				'BuoiToi' => $BuoiToi);
		return $this->db->update('menu',$data);
	} 
	
	public function delete_menu_by_id($id){
		$this->load->database();
		$this->db->from('menu');
		$this->db->where('id',$id);
		return $this->db->delete('menu');
	}}