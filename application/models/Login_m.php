<?php
class Login_m extends CI_Model
{
    function load_user()
    {
        $this->load->database();
        $this->db->from('nguoidung');
        $list = $this->db->get()->result();
        return $list;
    }
    function check_login($user,$pass)
    {
        $this->load->database();
        $this->db->from('nguoidung');
        $this->db->where('tendangnhap', $user);
        $this->db->where('matkhau', md5($pass));
        $login = $this->db->get()->result();
        if (is_array($login) && count($login) == 1) {
            $_SESSION['user']= $login[0];
            return true;
        }
        return false;
    }
    function get_user_by_email($email)
    {
        $this->load->database();
        $this->db->from('user');
        $this->db->where('email', $email);
        return $this->db->get()->result()[0];
    }
}
