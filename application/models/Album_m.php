<?php
class Album_m extends CI_Model
{
	
	function load_all_album()
	{
		$this->load->database();
		$this->db->select('a.*,l.Tenlop as tenlop');			
		$this->db->from('album a');
		$this->db->join('lop l ', 'l.id = a.Malop');		
		
		$list = $this->db->get()->result();
		return $list;
	}
	
	
	function get_album_by_id($id)
	{
		$this->load->database();
		$this->db->from('album');
		$this->db->where('id',$id);
		$list = $this->db->get()->result()[0];
		return $list;
	}
	
	
	function add_album($Tenalbum, $Giodang, $Malop, $Mota, $Soluong){
		
		$this->load->database();
		$this->db->from('album');
		$data = array(
				'Tenalbum' => $Tenalbum, 
				'Giodang' => $Giodang, 
				'Malop' => $Malop, 
				'Mota' => $Mota, 
				'Soluong' => $Soluong);
		return $this->db->insert('album',$data);
	}
	
	function edit_album_by_id($id, $Tenalbum, $Giodang, $Malop, $Mota, $Soluong){
		$this->load->database();
		$this->db->from('album');
		$this->db->where('id',$id);
		$data = array(
				'Tenalbum' => $Tenalbum, 
				'Giodang' => $Giodang, 
				'Malop' => $Malop, 
				'Mota' => $Mota, 
				'Soluong' => $Soluong);
		return $this->db->update('album',$data);
	} 
	
	public function delete_album_by_id($id){
		$this->load->database();
		$this->db->from('album');
		$this->db->where('id',$id);
		return $this->db->delete('album');
	}
	//code
}