<?php
class Diemdanh_m extends CI_Model
{
	
	function load_all_diemdanh()
	{
		$this->load->database();
		$this->db->select('d.*,h.Hoten as tenhs,l.Tenlop as tenlop');		
		$this->db->from('diemdanh d');
		$this->db->join('hocsinh h ', 'h.id = d.Mahocsinh');
		$this->db->join('lop l ', 'l.id = d.Malop');		
		$list = $this->db->get()->result();
		return $list;
	}
	
	
	function get_diemdanh_by_id($id)
	{
		$this->load->database();
		$this->db->from('diemdanh');
		$this->db->where('id',$id);
		$list = $this->db->get()->result()[0];
		return $list;
	}
	
	
	function add_diemdanh($Mahocsinh, $Malop, $Giodiemdanh, $Trangthai){
		
		$this->load->database();
		$this->db->from('diemdanh');
		$data = array(
				'Mahocsinh' => $Mahocsinh, 
				'Malop' => $Malop, 
				'Giodiemdanh' => $Giodiemdanh, 
				'Trangthai' => $Trangthai);
		return $this->db->insert('diemdanh',$data);
	}
	
	function edit_diemdanh_by_id($id, $Mahocsinh, $Malop, $Giodiemdanh, $Trangthai){
		$this->load->database();
		$this->db->from('diemdanh');
		$this->db->where('id',$id);
		$data = array(
				'Mahocsinh' => $Mahocsinh, 
				'Malop' => $Malop, 
				'Giodiemdanh' => $Giodiemdanh, 
				'Trangthai' => $Trangthai);
		return $this->db->update('diemdanh',$data);
	} 
	
	public function delete_diemdanh_by_id($id){
		$this->load->database();
		$this->db->from('diemdanh');
		$this->db->where('id',$id);
		return $this->db->delete('diemdanh');
	}}