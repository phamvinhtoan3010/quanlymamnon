<?php
class Suckhoe_m extends CI_Model
{
	
	function load_all_suckhoe()
	{
		$this->load->database();
		$this->db->select('s.*,h.Hoten as tenhs');			
		$this->db->from('suckhoe s');
		$this->db->join('hocsinh h ', 'h.id = s.Mahocsinh');
		$list = $this->db->get()->result();
		return $list;
	}
	
	
	function get_suckhoe_by_id($id)
	{
		$this->load->database();
		$this->db->from('suckhoe');
		$this->db->where('id',$id);
		$list = $this->db->get()->result()[0];
		return $list;
	}
	
	
	function add_suckhoe($Ngaykiemtra, $Mahocsinh, $Chieucao, $Cannang){
		
		$this->load->database();
		$this->db->from('suckhoe');
		$data = array(
				'Ngaykiemtra' => $Ngaykiemtra, 
				'Mahocsinh' => $Mahocsinh, 
				'Chieucao' => $Chieucao, 
				'Cannang' => $Cannang);
		return $this->db->insert('suckhoe',$data);
	}
	
	function edit_suckhoe_by_id($id, $Ngaykiemtra, $Mahocsinh, $Chieucao, $Cannang){
		$this->load->database();
		$this->db->from('suckhoe');
		$this->db->where('id',$id);
		$data = array(
				'Ngaykiemtra' => $Ngaykiemtra, 
				'Mahocsinh' => $Mahocsinh, 
				'Chieucao' => $Chieucao, 
				'Cannang' => $Cannang);
		return $this->db->update('suckhoe',$data);
	} 
	
	public function delete_suckhoe_by_id($id){
		$this->load->database();
		$this->db->from('suckhoe');
		$this->db->where('id',$id);
		return $this->db->delete('suckhoe');
	}

	public function get_suckhoe_with_hocsinh_info() {
		$this->load->database();
	
		$query = "SELECT hs.Hoten, hs.Nickname, hs.Gioitinh, hs.Ngaysinh, sk.Chieucao, sk.Cannang
				FROM suckhoe sk
				JOIN hocsinh hs ON sk.Mahocsinh = hs.id";
		$list = $this->db->query($query)->result();

		return $list;
	}

}