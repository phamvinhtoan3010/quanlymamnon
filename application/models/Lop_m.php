<?php
class Lop_m extends CI_Model
{
	
	function load_all_lop()
	{
		$this->load->database();
		$this->db->from('lop');
		$list = $this->db->get()->result();
		return $list;
	}
	
	
	function get_lop_by_id($id)
	{
		$this->load->database();
		$this->db->from('lop');
		$this->db->where('id',$id);
		$list = $this->db->get()->result()[0];
		return $list;
	}
	
	
	function add_lop($Tenlop, $Giaovien1, $Giaovien2, $Giaovien3){
		
		$this->load->database();
		$this->db->from('lop');
		$data = array(
				'Tenlop' => $Tenlop, 
				'Giaovien1' => $Giaovien1, 
				'Giaovien2' => $Giaovien2, 
				'Giaovien3' => $Giaovien3);
		return $this->db->insert('lop',$data);
	}
	
	function edit_lop_by_id($id, $Tenlop, $Giaovien1, $Giaovien2, $Giaovien3){
		$this->load->database();
		$this->db->from('lop');
		$this->db->where('id',$id);
		$data = array(
				'Tenlop' => $Tenlop, 
				'Giaovien1' => $Giaovien1, 
				'Giaovien2' => $Giaovien2, 
				'Giaovien3' => $Giaovien3);
		return $this->db->update('lop',$data);
	} 
	
	public function delete_lop_by_id($id){
		$this->load->database();
		$this->db->from('lop');
		$this->db->where('id',$id);
		return $this->db->delete('lop');
	}}