<?php
class Hocsinh_m extends CI_Model
{
	
	function load_all_hocsinh()
	{
		$this->load->database();
		$this->db->select('h.*, u.Hoten as tenph, l.Tenlop as tenlop');	
		$this->db->from('hocsinh h');
		$this->db->join('nguoidung u', 'u.id  = h.Maphuhuynh');
		$this->db->join('lop l', 'l.id  = h.Malop');
		
		$list = $this->db->get()->result();
		return $list;
	}
	
	
	function get_hocsinh_by_id($id)
	{
		$this->load->database();
		$this->db->from('hocsinh');
		$this->db->where('id',$id);
		$list = $this->db->get()->result()[0];
		return $list;
	}
	
	
	function add_hocsinh($Hoten, $Nickname, $Gioitinh, $Ngaysinh, $Maphuhuynh, $Malop){
		
		$this->load->database();
		$this->db->from('hocsinh');
		$data = array(
				'Hoten' => $Hoten, 
				'Nickname' => $Nickname, 
				'Gioitinh' => $Gioitinh, 
				'Ngaysinh' => $Ngaysinh, 
				'Maphuhuynh' => $Maphuhuynh, 
				'Malop' => $Malop);
		return $this->db->insert('hocsinh',$data);
	}
	
	function edit_hocsinh_by_id($id, $Hoten, $Nickname, $Gioitinh, $Ngaysinh, $Maphuhuynh, $Malop){
		$this->load->database();
		$this->db->from('hocsinh');
		$this->db->where('id',$id);
		$data = array(
				'Hoten' => $Hoten, 
				'Nickname' => $Nickname, 
				'Gioitinh' => $Gioitinh, 
				'Ngaysinh' => $Ngaysinh, 
				'Maphuhuynh' => $Maphuhuynh, 
				'Malop' => $Malop);
		return $this->db->update('hocsinh',$data);
	} 
	
	public function delete_hocsinh_by_id($id){
		$this->load->database();
		$this->db->from('hocsinh');
		$this->db->where('id',$id);
		return $this->db->delete('hocsinh');
	}}