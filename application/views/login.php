<!DOCTYPE html>
<html>

<head>
    <?php include("tm_head.php"); ?>
</head>

<body background="<?=base_url() ?>layout/images/blue.jpg">
    <!-- Page Loader -->
	
    <!-- #Top Bar -->
    <section class="content">
       <div class="container-fluid">
            <div class="block-header">
                <h2></h2>
            </div>
			<div class="row clearfix m-t-40">
				<div class="col-lg-1 hide-md hide-sm hide-xs"></div>
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header"><h2>ĐĂNG NHẬP</h2>
                        </div>
                        <div class="body">
                            <form class="form-horizontal" id="dang_nhap" name="dang_nhap" method="post" action="<?=base_url()?>login/login_user">
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 form-control-label">
                                        <label for="ten_dang_nhap">Tên đăng nhập</label>
                                    </div>
                                    <div class="col-lg-10 col-md-9 col-sm-9 col-xs-8">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="ten_dang_nhap" name="ten_dang_nhap" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 form-control-label">
                                        <label for="mat_khau">Mật khẩu</label>
                                    </div>
                                    <div class="col-lg-10 col-md-9 col-sm-9 col-xs-8">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="password" id="mat_khau" name="mat_khau" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-3 col-sm-offset-3 col-xs-offset-4">
                                        <a href="<?=base_url()?>login/recover">Quên mật khẩu?</a>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-3 col-sm-offset-3 col-xs-offset-4">
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect btn-lg">ĐĂNG NHẬP</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
				<div class="col-lg-2 hide-md hide-sm hide-xs"></div>
            </div>
    </section>
    <?php include("tm_foot.php"); ?>

    
</body>

</html>