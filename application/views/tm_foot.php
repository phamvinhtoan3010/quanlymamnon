<!-- Jquery Core Js -->
<script src="<?= base_url() ?>layout/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="<?= base_url() ?>layout/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="<?= base_url() ?>layout/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Input Mask Plugin Js -->
<script src="<?= base_url() ?>layout/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

<!-- Multi Select Plugin Js -->
<script src="<?= base_url() ?>layout/plugins/multi-select/js/jquery.multi-select.js"></script>

<!-- Jquery Spinner Plugin Js -->
<script src="<?= base_url() ?>layout/plugins/jquery-spinner/js/jquery.spinner.js"></script>

<!-- Bootstrap Tags Input Plugin Js -->
<script src="<?= base_url() ?>layout/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?= base_url() ?>layout/plugins/node-waves/waves.js"></script>

<!-- SweetAlert -->
<link href="<?= base_url() ?>layout/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

<!-- Jquery CountTo Plugin Js -->
<script src="<?= base_url() ?>layout/plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Jquery ScrollTo Plugin Js -->
<script src="<?= base_url()?>layout/plugins/jquery-scrollto/jquery.scrollTo.min.js"></script>

<!-- ChartJs -->
<script src="<?= base_url() ?>layout/plugins/chartjs/Chart.bundle.js"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="<?= base_url()?>layout/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url()?>layout/plugins/jquery-datatable/dataTables.responsive.min.js"></script>
<script src="<?= base_url()?>layout/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

<!-- ck -->
<script src="<?= base_url() ?>layout/ck/ckeditor/ckeditor.js"></script>

<!-- Custom Js -->
<script src="<?= base_url() ?>layout/js/admin.js"></script>
<script src="<?= base_url() ?>layout/js/pages/forms/advanced-form-elements.js"></script>

<script src="<?= base_url() ?>dist/jquery.fancybox.min.js"></script>

<script>
    $(function() {
        $('.sidebar .menu').scrollTo('.active');
    });
</script>

<?php include("tm_message.php"); ?>