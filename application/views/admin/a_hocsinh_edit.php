<?php include("tm_head.php"); ?>

    <body class="theme-deep-orange">
        <?php include("tm_topbar.php"); ?>
            <?php include("tm_navi.php"); ?>

                <!-- Add content here -->

                <body class="theme-red">
                    <?php include("tm_topbar.php"); ?>
                        <?php include("tm_navi.php"); ?>
                            <section class="content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8">
                                            <div class="card">
                                                <div class="header">
                                                    <h2 class="chon-lop">Sửa học sinh <?= $list->Hoten ?>
                        </h2>
                                                </div>
                                                <div class="body">
                                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                        <li role="presentation" class="active"><a href="#them-tu-form" data-toggle="tab">SỬA TỪ FORM</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane fade in active" id="them-tu-form">
                                                            <form method="POST" action="<?= base_url("admin/hocsinh/form_edit_hocsinh ") ?>">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group label-floating">
                                                                            <b>Họ tên *</b>
                                                                            <div class="form-line">
                                                                                <input type="text" value="<?= $list->Hoten ?>" id="Hoten" name="Hoten" class="form-control" required>
                                                                            </div>
                                                                            <div class="form-line" style="display:none;">
                                                                                <input type="text"  value="<?= $list->id ?>" id="id" name="id" class="form-control" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group label-floating">
                                                                            <b>Nickname</b>
                                                                            <div class="form-line">
                                                                                <input type="text" value="<?= $list->Nickname ?>" id="Nickname" name="Nickname" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group label-floating">
                                                                            <b>Giới tính *</b>
                                                                            <div class="form-line">
                                                                                <select class="form-control" name="Gioitinh" id="Gioitinh" required>
                                                                                    <?php 
                                                                                if($list->Gioitinh == "0"){ ?>
                                                                                        <option value="0" selected>Nam</option>
                                                                                        <option value="1">Nữ</option>
                                                                                        <?php 
                                                                                }
                                                                                else
                                                                                {
                                                                                ?>
                                                                                            <option value="0">Nam</option>
                                                                                            <option value="1" selected>Nữ</option>
                                                                                            <?php
                                                                                }
                                                                                ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group label-floating">
                                                                            <b>Ngày sinh</b>
                                                                            <div class="form-line">
                                                                                <input type="date" value="<?= mdate('%Y-%m-%d', mysql_to_unix($list->Ngaysinh)); ?>" id="Ngaysinh" name="Ngaysinh" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group label-floating">
                                                                            <b>Tên lớp *</b>
                                                                            <div class="form-line">
                                                                                <select class="form-control" name="Malop" id="Malop" required>
                                                                                    <?php if(isset($listLop)){ foreach($listLop as $tmp){ ?>
                                                                                        <option value="<?= $tmp->Malop ?>" <?php if($tmp->Malop == $listLop->Malop) echo "selected" ?> >
                                                                                            <?=  $tmp->Tenlop ?>
                                                                                        </option>
                                                                                        <?php }} ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group label-floating">
                                                                            <b>Tên phụ huynh *</b>
                                                                            <div class="form-line">
                                                                                <select class="form-control" name="Maphuhuynh" id="Maphuhuynh" required>
                                                                                    <?php if(isset($listPH)){ foreach($listPH as $tmp){ ?>
                                                                                        <option value="<?= $tmp->Maphuhuynh ?>" <?php if($tmp->Maphuhuynh == $listPH->Maphuhuynh) echo "selected" ?> >
                                                                                            <?=  $tmp->Hoten ?>
                                                                                        </option>
                                                                                        <?php }} ?>
                                                                                    </div>
                                                                            </div>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <button type="submit" id="nut-tao-moi" class="btn btn-block nen-xanh-dam chu-trang waves-effect">LƯU THAY ĐỔI</button>
                                                                        </div>
                                                                    </div>
                                                                                    </div>
                                                            </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2"></div>
                                            </div>
                                        </div>
                            </section>
                            <!-- #END Add content here -->

                            <?php include("tm_foot.php"); ?>
                </body>
                <script>
                    function checkalert(id) {
                        swal({
                                title: "Bạn đã đã chắn chắn?",
                                text: "Hành động này sẽ xóa tất cả các dữ liệu liên quan, và không thể phục hồi",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Đồng ý!",
                                closeOnConfirm: false
                            },
                            function() {
                                swal("Thành công!", "Dữ liệu của bạn đã được xóa!", "success");
                                window.location.href = "<?= base_url() ?>admin/hocsinh/delete_hocsinh/" + id;
                            });
                    }
                </script>

                </html>