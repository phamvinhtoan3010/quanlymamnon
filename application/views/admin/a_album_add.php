<style>
.prod_image img{
    height:100px;
    border: 3px  solid #ddd;
    border-radius:8px;
    padding:5px;
}
.prod_image a{
    margin-left:10px;
}

.prod_image input{
    display:none;
}
.prod_image{
    margin-left:5%;
    
}
.list_image{
    width:80%;
    margin-left:10%;
}

</style>

<?php include("tm_head.php"); ?>

<body class="theme-deep-orange">
    <?php include("tm_topbar.php"); ?>
    <?php include("tm_navi.php"); ?>
    
    <!-- Add content here -->
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Hình ảnh</h2>
            </div>

            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Thêm hình ảnh mới</h2>
                        </div>
                         
                        <div class="body">
                            <form action="<?= base_url("admin/gen_banner/form_add_gen_banner"); ?>" method="post"  >
                                <div class="row clearfix" id="form-body">
                                    <div class="col-sm-12">
                                        <label for="image">hình ảnh</label>
                                        <div class="prod_image">
                                            <img src="<?=base_url();?>images/camera.png" alt="" id="prev_img" >
                                            <a href="<?=base_url();?>filemanager/dialog.php?type=1&field_id=image" data-fancybox data-type="iframe" class = "btn bg-blue btn-raised waves-effect waves-light  fancy"   >Browse </a>
                                            <input type="text" class="form-control" id ="image" name="image">
                                        </div>
                                    </div>  

                                    
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-block btn-lg bg-red waves-effect">SAVE</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- #END Add content here -->

    <?php include("tm_foot.php"); ?>
</body>


<script>
   
   $(document).ready(function(){
        $('.fancy').fancybox({	
            'width'		: 900,
            'height'	: 600,
            'type'		: 'iframe',
            'autoScale'    	: false
        });
    });
    function responsive_filemanager_callback(field_id){
        var image = $('#' + field_id).val();
        $('#prev_img').attr('src',image);
    }

</script>


</html>