<?php include("tm_head.php"); ?>

<body class="theme-deep-orange">
    <?php include("tm_topbar.php"); ?>
    <?php include("tm_navi.php"); ?>
    
    <!-- Add content here -->
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>THỐNG KÊ</h2>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">supervisor_account</i>
                        </div>
                        <div class="content">
                            <div class="text">NGƯỜI DÙNG</div>
                            <div class="number count-to" data-from="0" data-to="<?= $nNguoidung ?>" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">face</i>
                        </div>
                        <div class="content">
                            <div class="text">HỌC SINH</div>
                            <div class="number count-to" data-from="0" data-to="<?= $nHocsinh ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">chrome_reader_mode</i>
                        </div>
                        <div class="content">
                            <div class="text">BÀI ĐĂNG</div>
                            <div class="number count-to" data-from="0" data-to="<?= $nBaidang ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">photo_library</i>
                        </div>
                        <div class="content">
                            <div class="text">ALBUM</div>
                            <div class="number count-to" data-from="0" data-to="<?= $nAlbum ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>CẢNH BÁO SỨC KHOẺ HỌC SINH</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tên HS</th>
                                            <th>Nickname</th>
                                            <th>Tình trạng cân nặng</th>
                                            <th>Tình trạng chiều cao</th>
                                        </tr>
                                    </thead>
                                    <tbody> <!-- bg-green / bg-light-blue / bg-orange / bg-red -->
                                        <?php if (isset($suckhoe)) { ?>
                                            <?php for ($i=0; $i < count($suckhoe); $i++) { ?>
                                                <tr>
                                                    <td><?= $i+1 ?></td>
                                                    <td><?= $suckhoe[$i]['fullname'] ?></td>
                                                    <td><?= $suckhoe[$i]['nickname'] ?></td>
                                                    <td>
                                                        <span class="label <?= $suckhoe[$i]['bg1'] ?>"><?= $suckhoe[$i]['weight'].' kg ('.$suckhoe[$i]['predict1'].")" ?></span>
                                                    </td>
                                                    <td>
                                                        <span class="label <?= $suckhoe[$i]['bg2'] ?>"><?= $suckhoe[$i]['height'].' cm ('.$suckhoe[$i]['predict2'].")" ?></span>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>
        </div>
    </section>
    <!-- #END Add content here -->

    <?php include("tm_foot.php"); ?>
    <script>
        $(function () {
            //Widgets count
            $('.count-to').countTo();

            //Sales count to
            $('.sales-count-to').countTo({
                formatter: function (value, options) {
                    return '$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, ' ').replace('.', ',');
                }
            });
        });


        var data = [], totalPoints = 110;
        function getRandomData() {
            if (data.length > 0) data = data.slice(1);

            while (data.length < totalPoints) {
                var prev = data.length > 0 ? data[data.length - 1] : 50, y = prev + Math.random() * 10 - 5;
                if (y < 0) { y = 0; } else if (y > 100) { y = 100; }

                data.push(y);
            }

            var res = [];
            for (var i = 0; i < data.length; ++i) {
                res.push([i, data[i]]);
            }

            return res;
        }
    </script>
</body>

</html>