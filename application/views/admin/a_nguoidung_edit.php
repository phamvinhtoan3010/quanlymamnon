<?php include("tm_head.php"); ?>

    <body class="theme-deep-orange">
        <?php include("tm_topbar.php"); ?>
            <?php include("tm_navi.php"); ?>

                <!-- Add content here -->

                <body class="theme-red">
                    <?php include("tm_topbar.php"); ?>
                        <?php include("tm_navi.php"); ?>
                            <section class="content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8">
                                            <div class="card">
                                                <div class="header">
                                                    <h2 class="chon-lop">Sửa người dùng <?= $list->Hoten ?>
							</h2>
                                                </div>
                                                <div class="body">
                                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                        <li role="presentation" class="active"><a href="#them-tu-form" data-toggle="tab">SỬA TỪ FORM</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane fade in active" id="them-tu-form">
                                                            <form method="POST" action="<?= base_url("admin/nguoidung/form_edit_nguoidung ") ?>">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group label-floating">
                                                                            <b>Tên đăng nhập*</b>
                                                                            <div class="form-line">
                                                                                <input type="text" value="<?= $list->tendangnhap ?>" id="tendangnhap" name="tendangnhap" class="form-control" required>
                                                                            </div>
                                                                            <div class="form-line" style="display:none">
                                                                                <input type="text" value="<?= $list->id ?>" id="id" name="id" class="form-control" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group label-floating">
                                                                            <b>Mật khẩu *</b>
                                                                            <div class="form-line">
                                                                                <input type="password" value="<?= $list->Matkhau ?>" id="Matkhau" name="Matkhau" class="form-control" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-9">
                                                                        <div class="form-group label-floating">
                                                                            <b>Họ tên *</b>
                                                                            <div class="form-line">
                                                                                <input type="text" value="<?= $list->Hoten ?>" id="Hoten" name="Hoten" class="form-control" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="form-group label-floating">
                                                                            <b>Giới tính *</b>
                                                                            <div class="form-line">
                                                                                <select class="form-control" name="Gioitinh" id="Gioitinh" required>
                                                                                    <?php 
														                                if($list->Gioitinh == "0"){ ?>
                                                                                        <option value="0" selected>Nam</option>
                                                                                        <option value="1">Nữ</option>
                                                                                        <?php 
                                                                                        }
                                                                                        else
                                                                                        {
														                                ?>
                                                                                            <option value="0">Nam</option>
                                                                                            <option value="1" selected>Nữ</option>
                                                                                        <?php
													                                	}
												                                    	?>

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group label-floating">
                                                                            <b>Email</b>
                                                                            <div class="form-line">
                                                                                <input type="text" value="<?= $list->Email ?>" id="Email" name="Email" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group label-floating">
                                                                            <b>Ngày sinh</b>
                                                                            <div class="form-line">
                                                                                <input type="date" value="<?= mdate('%Y-%m-%d', mysql_to_unix($list->Ngaysinh)); ?>" id="Ngaysinh" name="Ngaysinh" class="form-control">
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-5">
                                                                        <div class="form-group label-floating">
                                                                            <b>Số điện thoại</b>
                                                                            <div class="form-line">
                                                                                <input type="text" value="<?= $list->SDT ?>" id="SDT" name="SDT" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <div class="form-group label-floating">
                                                                            <b>Quyền truy cập *</b>
                                                                            <div class="form-line">
                                                                                <select class="form-control" name="Quyentruycap" id="Quyentruycap" required>
                                                                                    <?php 
													                            	    if($list->Quyentruycap == "1"){ ?>
                                                                                        <option value="1" selected>Người quản lý</option>
                                                                                        <option value="2">Giáo viên</option>
                                                                                        <option value="3">Phụ huynh</option>
                                                                                        <?php 
                                                                                        }
                                                                                        else if ($list->Quyentruycap == "2")
                                                                                        {
														                            	?>
                                                                                            <option value="1">Người quản lý</option>
                                                                                            <option value="2" selected>Giáo viên</option>
                                                                                            <option value="3">Phụ huynh</option>
                                                                                            <?php 
                                                                                        }
                                                                                        else 
                                                                                        {
                                                                                        ?>
                                                                                            <option value="1">Người quản lý</option>
                                                                                            <option value="2">Giáo viên</option>
                                                                                            <option value="3" selected>Phụ huynh</option>
                                                                               
                                                                                        <?php 
                                                                                        }
                                                                                        ?>
                                                                                 </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group label-floating">
                                                                    <b>Địa chỉ</b>
                                                                    <div class="form-line">
                                                                        <!-- <input type="text" id="address" name="address" class="form-control" required> -->
                                                                        <textarea type="text" class="form-control" id="Diachi" name="Diachi" required>
                                                                            <?= $list->Diachi ?>
                                                                        </textarea>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <button type="submit" id="nut-tao-moi" class="btn btn-block nen-xanh-dam chu-trang waves-effect">LƯU THAY ĐỔI</button>
                                                            </div>
                                                        </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                </div>
                            </section>
                            <!-- #END Add content here -->

                            <?php include("tm_foot.php"); ?>
                </body>
                <script>
                    function checkalert(id) {
                        swal({
                                title: "Bạn đã đã chắn chắn?",
                                text: "Hành động này sẽ xóa tất cả các dữ liệu liên quan, và không thể phục hồi",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Đồng ý!",
                                closeOnConfirm: false
                            },
                            function() {
                                swal("Thành công!", "Dữ liệu của bạn đã được xóa!", "success");
                                window.location.href = "<?= base_url() ?>admin/nguoidung/delete_nguoidung/" + id;
                            });
                    }
                </script>

                </html>