<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Dashboard | HỆ THỐNG QUẢN LÝ TRƯỜNG MẦM NON</title>
    <!-- Favicon-->
    <link rel="icon" href="<?= base_url() ?>favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= base_url() ?>layout/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="<?= base_url() ?>layout/plugins/multi-select/css/multi-select.css" rel="stylesheet">

    <!-- Bootstrap Spinner Css -->
    <link href="<?= base_url() ?>layout/plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">

    <!-- Bootstrap Tagsinput Css -->
    <link href="<?= base_url() ?>layout/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="<?= base_url() ?>layout/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Waves Effect Css -->
    <link href="<?= base_url() ?>layout/plugins/node-waves/waves.css" rel="stylesheet" />
    
    <!-- SweetAlert -->
    <script src="<?= base_url() ?>layout/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Animation Css -->
    <link href="<?= base_url() ?>layout/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?= base_url()?>layout/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?= base_url()?>layout/plugins/jquery-datatable/skin/bootstrap/css/responsive.dataTables.min.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?= base_url() ?>layout/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= base_url() ?>layout/css/themes/all-themes.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>dist/jquery.fancybox.min.css">
    
</head>