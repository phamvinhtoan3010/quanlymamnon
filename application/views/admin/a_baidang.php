<?php include("tm_head.php"); ?>

    <body class="theme-deep-orange">
        <?php include("tm_topbar.php"); ?>
            <?php include("tm_navi.php"); ?>

                <!-- Add content here -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="block-header">
                            <h2>Quản lý bài đăng</h2>
                        </div>

                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="card">
                                    <div class="header">
                                        <a class="btn bg-deep-orange btn-raised pull-right m-t--10 waves-effect waves-light" href="<?php echo base_url()."them-baidang" ?>">
                                            <i class="material-icons">add</i>
                                        </a>
                                        <h2>Danh sách bài viết</h2>
                                    </div>
                                    <div class="body">
                                        <table id="mytable" class="table table-striped table-bordered table-hover display responsive no-wrap dataTable no-footer" style="margin-top:10px">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Lớp</th>
                                                    <th>Người dùng</th>
                                                    <th>Nội dung</th>
                                                    <th>Ngày đăng</th>
                                                    <th>Tác vụ</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (isset($list)){ foreach($list as $item) { ?>
                                                    <tr>
                                                        <td>
                                                            <?= $item->id ?>
                                                        </td>
                                                        <td>
                                                            <?= $item->tenlop ?>
                                                        </td>
                                                        <td>
                                                            <?= $item->tennd ?>
                                                        </td>
                                                        <td>
                                                            <?= substr($item->Noidung,0,100)." . . . " ?>
                                                        </td>
                                                        <td>
                                                            <?=date("d-m-Y", strtotime($item->Ngaydang)); ?>
                                                            
                                                        </td>

                                                        <td>
                                                            <a href="<?= base_url() ?>sua-baidang/<?= $item->id ?>" class="btn btn-xs btn-info btn-raised"><i class="material-icons">mode_edit</i></a>
                                                            <a onclick="checkalert(<?= $item->id ?>)" class="btn btn-xs btn-danger btn-raised"><i class="material-icons">delete</i></a>
                                                        </td>
                                                    </tr>
                                                    <?php } }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- #END Add content here -->

                <?php include("tm_foot.php"); ?>
    </body>
    <script>
        function checkalert(id) {
            swal({
                    title: "Bạn đã đã chắn chắn?",
                    text: "Hành động này sẽ xóa tất cả các dữ liệu liên quan, và không thể phục hồi",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Đồng ý!",
                    closeOnConfirm: false
                },
                function() {
                    swal("Thành công!", "Dữ liệu của bạn đã được xóa!", "success");
                    window.location.href = "<?= base_url() ?>admin/baidang/delete_baidang/" + id;
                });
        }
    </script>

    </html>