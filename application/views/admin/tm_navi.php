<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="<?= base_url() ?>layout/images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php if(isset($_SESSION['user'])) echo $_SESSION['user']->Hoten; ?></div>
                <div class="email"><?php if(isset($_SESSION['user'])) echo $_SESSION['user']->Email; ?></div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="javascript:void(0);">
                                <i class="material-icons">person</i>Profile</a>
                        </li>
                        <li role="seperator" class="divider"></li>
                        <li>
                            <a href="javascript:void(0);">
                                <i class="material-icons">group</i>Followers</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <i class="material-icons">shopping_cart</i>Sales</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <i class="material-icons">favorite</i>Likes</a>
                        </li>
                        <li role="seperator" class="divider"></li>
                        <li>
                            <a href="<?= base_url("login/logout") ?>">
                                <i class="material-icons">input</i>Sign Out</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">ĐIỀU HƯỚNG CHÍNH</li>
                <li class="<?php  if(isset($_SESSION['navi']) && $_SESSION['navi']=="tongquan"){echo "active";}  ?>">
                    <a href="<?= base_url() ?>tong-quan">
                        <i class="material-icons">developer_board</i>
                        <span>Tổng quan</span>
                    </a>
                </li>

                <li class="<?php  if(isset($_SESSION['navi']) && strpos($_SESSION['navi'], 'qltaikhoan') !== FALSE){echo "active";}  ?>">
                    <a href="<?= base_url() ?>ds-tai-khoan">
                        <i class="material-icons">supervisor_account</i>
                        <span>Quản lý tài khoản</span>
                    </a>
                </li>

                <li class="<?php  if(isset($_SESSION['navi']) && strpos($_SESSION['navi'], 'qllop') !== FALSE){echo "active";}  ?>">
                    <a href="<?= base_url() ?>ds-lop">
                        <i class="material-icons">widgets</i>
                        <span>Quản lý lớp</span>
                    </a>
                </li>

                <li class="<?php  if(isset($_SESSION['navi']) && strpos($_SESSION['navi'], 'qltre') !== FALSE){echo "active";}  ?>">
                    <a href="<?= base_url() ?>ds-hocsinh">
                        <i class="material-icons">face</i>
                        <span>Quản lý học sinh</span>
                    </a>
                </li>

                <li class="<?php  if(isset($_SESSION['navi']) && strpos($_SESSION['navi'], 'qldiemdanh') !== FALSE){echo "active";}  ?>">
                    <a href="<?= base_url() ?>ds-diemdanh">
                        <i class="material-icons">playlist_add_check</i>
                        <span>Quản lý điểm danh</span>
                    </a>
                </li>
                
                <li class="<?php  if(isset($_SESSION['navi']) && strpos($_SESSION['navi'], 'qlsuckhoe') !== FALSE){echo "active";}  ?>">
                    <a href="<?= base_url() ?>ds-suckhoe">
                        <i class="material-icons">local_hospital</i>
                        <span>Quản lý sức khỏe</span>
                    </a>
                </li>

                <li class="<?php  if(isset($_SESSION['navi']) && strpos($_SESSION['navi'], 'qlbaiviet') !== FALSE){echo "active";}  ?>">
                    <a href="<?= base_url() ?>ds-baidang">
                        <i class="material-icons">chrome_reader_mode</i>
                        <span>Quản lý bài đăng</span>
                    </a>
                </li>

                <li class="<?php  if(isset($_SESSION['navi']) && strpos($_SESSION['navi'], 'qlalbum') !== FALSE){echo "active";}  ?>">
                    <a href="<?= base_url() ?>ds-album">
                        <i class="material-icons">photo_library</i>
                        <span>Quản lý album/ảnh</span>
                    </a>
                </li>

                        
                <li class="<?php  if(isset($_SESSION['navi']) && strpos($_SESSION['navi'], 'qlthucdon') !== FALSE){echo "active";}  ?>">
                    <a href="<?= base_url() ?>ds-thucdon">
                        <i class="material-icons">restaurant</i>
                        <span>Quản lý thực đơn</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
       
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>