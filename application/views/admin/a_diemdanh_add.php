<?php include("tm_head.php"); ?>

    <body class="theme-deep-orange">
        <?php include("tm_topbar.php"); ?>
            <?php include("tm_navi.php"); ?>

                <!-- Add content here -->
                <body class="theme-red">
                    <?php include("tm_topbar.php"); ?>
                        <?php include("tm_navi.php"); ?>
                            <section class="content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8">
                                            <div class="card">
                                                <div class="header">
                                                <h2 class="chon-lop">ĐIỂM DANH
                                                </h2>
                                                </div>
                                                <div class="body">
                                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                        <li role="presentation" class="active"><a href="#them-tu-form" data-toggle="tab">THÊM TỪ FORM</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane fade in active" id="them-tu-form">
                                                            <form method="POST" action="<?= base_url("admin/điemdanh/điemdanh_add") ?>">
                                                            <div class="col-sm-4">
                                                                        <div class="form-group label-floating">
                                                                            <b>Mã lớp *</b>
                                                                            <div class="form-line">
                                                                                <select class="form-control" name="Malop" id="Malop" required>
                                                                                    <?php for($i=0;$i< count($listLop);$i++){ ?>
                                                                                        <option value="<?= $listLop[$i]->id;?>">
                                                                                            <?= $listLop[$i]->Tenlop;?>
                                                                                        </option>
                                                                                        <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <button type="submit" id="nut-tao-moi" class="btn btn-block btn-lg nen-xanh-dam chu-trang waves-effect">LƯU THAY ĐỔI</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                </div>
                            </section>
                <!-- #END Add content here -->

                <?php include("tm_foot.php"); ?>
    </body>
    <script>
        function checkalert(id) {
            swal({
                    title: "Bạn đã đã chắn chắn?",
                    text: "Hành động này sẽ xóa tất cả các dữ liệu liên quan, và không thể phục hồi",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Đồng ý!",
                    closeOnConfirm: false
                },
                function() {
                    swal("Thành công!", "Dữ liệu của bạn đã được xóa!", "success");
                    window.location.href = "<?= base_url() ?>admin/diemdanh/delete_diemdanh/" + id;
                });
        }
    </script>

    </html>