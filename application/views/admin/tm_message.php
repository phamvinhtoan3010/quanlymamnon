<script src="<?php echo base_url();?>layout/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="<?php echo base_url();?>layout/js/pages/ui/notifications.js"></script>
   
<?php
	if(isset($_SESSION["THONGBAO"])){
		// print_r($_SESSION["THONGBAO"]); return;
?>
<script>	 

$(function(){
	var icon = "<?= $_SESSION["THONGBAO"]['icon']; ?>"; 

	var title = "<?= $_SESSION["THONGBAO"]['title']; ?>";
	var message = "<?= $_SESSION["THONGBAO"]['message']; ?>";
	var url = "<?= $_SESSION["THONGBAO"]['url']; ?>"; //url noti
	var type = "<?= $_SESSION["THONGBAO"]['type']; ?>";//success, danger, info, warning
	shownoti(icon,title,message,url,type);
	
});

</script>
<?php
	unset($_SESSION["THONGBAO"]);
} ?>