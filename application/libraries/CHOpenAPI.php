<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CHOpenAPI {
    protected $CI;
    private $token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OGQ4OWY1ZTBkYzQwMmRiMjkwMWU0NWYiLCJ1c2VybmFtZSI6ImJpbmgiLCJwYXNzd29yZCI6IjgzYWE0MzIxYmE2ZjBjMGU3NDYwMGU5YWMxZTgwNjg0IiwiZnVsbG5hbWUiOiJQaOG6oW0gVmnhu4d0IELDrG5oIiwiZ2VuZGVyIjoiMCIsImJpcnRoZGF5IjoiMjAvMDkvMjAxNyIsInBob25lIjoiMDE2NTU3MzA3MDQiLCJlbWFpbCI6Im1ya2F6YW5za3lAZ21haWwuY29tIiwiYWRkcmVzcyI6IjI4MCBBbiBEdW9uZyBWdW9uZywgd2FyZCA0LCBkaXN0cmljdCA1LCBIQ01DIiwicGVybWlzc2lvbiI6MCwic2Nob29sIjoiZnBvIiwiZGJOYW1lIjoiY2hpbGRodWIiLCJiYXNldXJsIjoiaHR0cDovL25vZGUwMy5mcG8udm4vaW1hZ2VzIiwiaW1ndXJsIjoiQzpcXEZQT1xcbm9kZTAzXFxpbWFnZXMiLCJyZWFsdGltZSI6Imh0dHA6Ly8yMDIuNzguMjI3LjczOjIwMjAiLCJhdmF0YXIiOiJib3kuanBnIiwiZmVhdHVyZSI6IlBPU1RfV0lUSE9VVF9WRVJJRlksTUVOVV9XSVRIX0RFVEFJTF9EQVRBLExOUFJPR1JBTV9XSVRIX0RFVEFJTF9EQVRBIiwiY2hpbGRMaXN0IjpbeyJfaWQiOiI1OGQ4YTgzMzBkYzQwMmRiMjkwMWU2MTMiLCJmdWxsbmFtZSI6Ik1haSBC4bqjbyBUcsOibSIsIm5pY2tuYW1lIjoiU3UiLCJnZW5kZXIiOiIxIiwiY2xhc3NJZCI6IjU4YmZjZWM3ODExNGVmMmMwNTAwMDAyOSIsImF2YXRhciI6Imh0dHA6Ly9ub2RlMDMuZnBvLnZuL2ltYWdlcy9jaGlsZC81OGQ4YTgzMzBkYzQwMmRiMjkwMWU2MTMuanBnIn0seyJfaWQiOiI1OGQ4YTg2MDBkYzQwMmRiMjkwMWU2MTkiLCJmdWxsbmFtZSI6IsSQaW5oIFRo4buLIFRyw6AgTWkiLCJuaWNrbmFtZSI6Ik1pIGN1dGUiLCJnZW5kZXIiOiIwIiwiY2xhc3NJZCI6IjU5YmIzNzU2MWQ3ZjE0M2M3Yzc5N2VlOCIsImF2YXRhciI6Imh0dHA6Ly9ub2RlMDMuZnBvLnZuL2ltYWdlcy9jaGlsZC9ib3kuanBnIn1dLCJpYXQiOjE1MjUzNzE2MDAsImV4cCI6MTUyNTk3NjQwMH0.3b5XVKjWSq5lWY-GGF45yors_uqi0v1NFGYUEDZN9pA';
    private $apiUrl = '202.78.227.73:2019/';

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
        $this->CI->load->helper('date');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    function arrayToString($data) {
        $chuoi="";
        foreach($data as $key => $value){
            $chuoi.=$key."=".$value."&";
        }
        return $chuoi;
    }

    // Call GHN API with cURL
    private function callAPI($apiName, $bodyData) {
        $curl = curl_init($this->apiUrl.$apiName);
        $headers = array();
        $headers[] = 'Accept:application/json';
        $headers[] = 'Content-Type:application/x-www-form-urlencoded';
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->arrayToString($bodyData));
        $curl_response = curl_exec($curl);
        $res = $curl_response;
        curl_close($curl);

        $res = json_decode($res);
        if ($res->exitcode == 1) {  //success
            return $res->data;
        } else {
            return $res->message;
        }
    }

    // ========== Get Districts - Get district list to create order ==========
    public function healthPredict($gender, $birthday, $weight, $height) {
        $res = $this->callAPI(
                'health/predict', 
                array(
                    "token" => $this->token,
                    "gender" => $gender,
                    "birthday" => $birthday,
                    "weight" => $weight,
                    "height" => $height
                )
        );

        return $res;
    }
    // ========== #END Get Districts - Get district list to create order ==========


}