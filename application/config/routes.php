<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// --------------------------------------------------------------------
$route['tong-quan'] = 'home/index';

$route['ds-tai-khoan'] = 'admin/Nguoidung/index';
$route['them-tai-khoan'] = 'admin/Nguoidung/add_nguoidung';
$route['sua-tai-khoan/(:any)'] = 'admin/Nguoidung/edit_nguoidung/$1';

$route['ds-lop'] = 'admin/Lop/index';
$route['them-lop'] = 'admin/Lop/add_lop';
$route['sua-lop/(:any)'] = 'admin/Lop/edit_lop/$1';

$route['ds-hocsinh'] = 'admin/Hocsinh/index';
$route['them-hocsinh'] = 'admin/Hocsinh/add_hocsinh';
$route['sua-hocsinh/(:any)'] = 'admin/Hocsinh/edit_hocsinh/$1';

$route['ds-diemdanh'] = 'admin/Diemdanh/index';
$route['them-diemdanh'] = 'admin/Diemdanh/add_diemdanh';
$route['sua-diemdanh/(:any)'] = 'admin/Diemdanh/edit_diemdanh/$1';

$route['ds-suckhoe'] = 'admin/Suckhoe/index';
$route['them-suckhoe'] = 'admin/Suckhoe/add_suckhoe';
$route['sua-suckhoe/(:any)'] = 'admin/Suckhoe/edit_suckhoe/$1';

$route['ds-baidang'] = 'admin/Baidang/index';
$route['them-baidang'] = 'admin/Baidang/add_baidang';
$route['sua-baidang/(:any)'] = 'admin/Baidang/edit_baidang/$1';

$route['ds-album'] = 'admin/Album/index';
$route['them-album'] = 'admin/Album/add_album';
$route['sua-album/(:any)'] = 'admin/Album/edit_album/$1';

$route['ds-thucdon'] = 'admin/Menu/index';
$route['them-thucdon'] = 'admin/Menu/add_menu';
$route['sua-thucdon/(:any)'] = 'admin/Menu/edit_menu/$1';

