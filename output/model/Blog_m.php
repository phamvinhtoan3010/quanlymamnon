<?php
class Blog_m extends CI_Model
{
	
	function load_all_blog()
	{
		$this->load->database();
		$this->db->from('blog');
		$list = $this->db->get()->result();
		return $list;
	}
	
	
	function get_blog_by_id($id)
	{
		$this->load->database();
		$this->db->from('blog');
		$this->db->where('id',$id);
		$list = $this->db->get()->result()[0];
		return $list;
	}
	
	
	function add_blog($title, $content, $image, $activeDate, $deactiveDate, $blogCategogyId, $author, $postDate, $nSeen, $nLove, $nShare, $tag, $isPin, $seo){
		
		$this->load->database();
		$this->db->from('blog');
		$data = array(
				'title' => $title, 
				'content' => $content, 
				'image' => $image, 
				'activeDate' => $activeDate, 
				'deactiveDate' => $deactiveDate, 
				'blogCategogyId' => $blogCategogyId, 
				'author' => $author, 
				'postDate' => $postDate, 
				'nSeen' => $nSeen, 
				'nLove' => $nLove, 
				'nShare' => $nShare, 
				'tag' => $tag, 
				'isPin' => $isPin, 
				'seo' => $seo);
		return $this->db->insert('blog',$data);
	}
	
	function edit_blog_by_id($id, $title, $content, $image, $activeDate, $deactiveDate, $blogCategogyId, $author, $postDate, $nSeen, $nLove, $nShare, $tag, $isPin, $seo){
		$this->load->database();
		$this->db->from('blog');
		$this->db->where('id',$id);
		$data = array(
				'title' => $title, 
				'content' => $content, 
				'image' => $image, 
				'activeDate' => $activeDate, 
				'deactiveDate' => $deactiveDate, 
				'blogCategogyId' => $blogCategogyId, 
				'author' => $author, 
				'postDate' => $postDate, 
				'nSeen' => $nSeen, 
				'nLove' => $nLove, 
				'nShare' => $nShare, 
				'tag' => $tag, 
				'isPin' => $isPin, 
				'seo' => $seo);
		return $this->db->update('blog',$data);
	} 
	
	public function delete_blog_by_id($id){
		$this->load->database();
		$this->db->from('blog');
		$this->db->where('id',$id);
		return $this->db->delete('blog');
	}}