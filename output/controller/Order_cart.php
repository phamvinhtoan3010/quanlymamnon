<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_cart extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
	
	public function index(){
		$this->load->model('Order_cart_m');
		$data['list']=$this->Order_cart_m->load_all_Order_cart();
		// print_r($data['list']);return;
		$this->load->view('admin/a_order_cart',$data);
	}
	public function add_order_cart()
	{
		$this->load->view('admin/a_order_cart_add');
	}
	public function form_add_order_cart($customerId, $voucherId, $ip, $userAgent, $createDate, $updateDate, $orderstatusId){
		$customerId = $this->input->post('customerId');
		$voucherId = $this->input->post('voucherId');
		$ip = $this->input->post('ip');
		$userAgent = $this->input->post('userAgent');
		$createDate = $this->input->post('createDate');
		$updateDate = $this->input->post('updateDate');
		$orderstatusId = $this->input->post('orderstatusId');
		
		$this->load->model('Order_cart_m');
		$this->Order_cart_m->add_order_cart($customerId, $voucherId, $ip, $userAgent, $createDate, $updateDate, $orderstatusId);
		redirect('/admin/order_cart/');
	}
	public function edit_order_cart($id)
	{
		$this->load->model('Order_cart_m');
		$data['list']=$this->Order_cart_m->get_order_cart_by_id($id);
		$this->load->view('admin/a_order_cart_edit',$data);
	}
	public function form_edit_order_cart($customerId, $voucherId, $ip, $userAgent, $createDate, $updateDate, $orderstatusId){
		$id = $this->input->post('$id');
		$customerId = $this->input->post('customerId');
		$voucherId = $this->input->post('voucherId');
		$ip = $this->input->post('ip');
		$userAgent = $this->input->post('userAgent');
		$createDate = $this->input->post('createDate');
		$updateDate = $this->input->post('updateDate');
		$orderstatusId = $this->input->post('orderstatusId');
		
		$this->load->model('Order_cart_m');
		$this->Order_cart_m->edit_order_cart_by_id($id, $customerId, $voucherId, $ip, $userAgent, $createDate, $updateDate, $orderstatusId);
		redirect('/admin/order_cart/');
	}
	public function delete_order_cart($id)
	{
		$this->load->model('Order_cart_m');
		$data['list']=$this->Order_cart_m->delete_order_cart_by_id($id);
		redirect('/admin/order_cart/');
	
	}
}