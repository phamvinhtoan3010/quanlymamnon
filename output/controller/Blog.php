<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
	
	public function index(){
		$this->load->model('Blog_m');
		$data['list']=$this->Blog_m->load_all_Blog();
		// print_r($data['list']);return;
		$this->load->view('admin/a_blog',$data);
	}
	public function add_blog()
	{
		$this->load->view('admin/a_blog_add');
	}
	public function form_add_blog($title, $content, $image, $activeDate, $deactiveDate, $blogCategogyId, $author, $postDate, $nSeen, $nLove, $nShare, $tag, $isPin, $seo){
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		$image = $this->input->post('image');
		$activeDate = $this->input->post('activeDate');
		$deactiveDate = $this->input->post('deactiveDate');
		$blogCategogyId = $this->input->post('blogCategogyId');
		$author = $this->input->post('author');
		$postDate = $this->input->post('postDate');
		$nSeen = $this->input->post('nSeen');
		$nLove = $this->input->post('nLove');
		$nShare = $this->input->post('nShare');
		$tag = $this->input->post('tag');
		$isPin = $this->input->post('isPin');
		$seo = $this->input->post('seo');
		
		$this->load->model('Blog_m');
		$this->Blog_m->add_blog($title, $content, $image, $activeDate, $deactiveDate, $blogCategogyId, $author, $postDate, $nSeen, $nLove, $nShare, $tag, $isPin, $seo);
		redirect('/admin/blog/');
	}
	public function edit_blog($id)
	{
		$this->load->model('Blog_m');
		$data['list']=$this->Blog_m->get_blog_by_id($id);
		$this->load->view('admin/a_blog_edit',$data);
	}
	public function form_edit_blog($title, $content, $image, $activeDate, $deactiveDate, $blogCategogyId, $author, $postDate, $nSeen, $nLove, $nShare, $tag, $isPin, $seo){
		$id = $this->input->post('$id');
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		$image = $this->input->post('image');
		$activeDate = $this->input->post('activeDate');
		$deactiveDate = $this->input->post('deactiveDate');
		$blogCategogyId = $this->input->post('blogCategogyId');
		$author = $this->input->post('author');
		$postDate = $this->input->post('postDate');
		$nSeen = $this->input->post('nSeen');
		$nLove = $this->input->post('nLove');
		$nShare = $this->input->post('nShare');
		$tag = $this->input->post('tag');
		$isPin = $this->input->post('isPin');
		$seo = $this->input->post('seo');
		
		$this->load->model('Blog_m');
		$this->Blog_m->edit_blog_by_id($id, $title, $content, $image, $activeDate, $deactiveDate, $blogCategogyId, $author, $postDate, $nSeen, $nLove, $nShare, $tag, $isPin, $seo);
		redirect('/admin/blog/');
	}
	public function delete_blog($id)
	{
		$this->load->model('Blog_m');
		$data['list']=$this->Blog_m->delete_blog_by_id($id);
		redirect('/admin/blog/');
	
	}
}