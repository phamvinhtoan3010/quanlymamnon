<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_address extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
	
	public function index(){
		$this->load->model('Order_address_m');
		$data['list']=$this->Order_address_m->load_all_Order_address();
		// print_r($data['list']);return;
		$this->load->view('admin/a_order_address',$data);
	}
	public function add_order_address()
	{
		$this->load->view('admin/a_order_address_add');
	}
	public function form_add_order_address($idOrder, $idcusAddress, $fullname, $phone){
		$idOrder = $this->input->post('idOrder');
		$idcusAddress = $this->input->post('idcusAddress');
		$fullname = $this->input->post('fullname');
		$phone = $this->input->post('phone');
		
		$this->load->model('Order_address_m');
		$this->Order_address_m->add_order_address($idOrder, $idcusAddress, $fullname, $phone);
		redirect('/admin/order_address/');
	}
	public function edit_order_address($id)
	{
		$this->load->model('Order_address_m');
		$data['list']=$this->Order_address_m->get_order_address_by_id($id);
		$this->load->view('admin/a_order_address_edit',$data);
	}
	public function form_edit_order_address($idOrder, $idcusAddress, $fullname, $phone){
		$id = $this->input->post('$id');
		$idOrder = $this->input->post('idOrder');
		$idcusAddress = $this->input->post('idcusAddress');
		$fullname = $this->input->post('fullname');
		$phone = $this->input->post('phone');
		
		$this->load->model('Order_address_m');
		$this->Order_address_m->edit_order_address_by_id($id, $idOrder, $idcusAddress, $fullname, $phone);
		redirect('/admin/order_address/');
	}
	public function delete_order_address($id)
	{
		$this->load->model('Order_address_m');
		$data['list']=$this->Order_address_m->delete_order_address_by_id($id);
		redirect('/admin/order_address/');
	
	}
}