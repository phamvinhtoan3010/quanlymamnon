<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cus_wishlist extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
	
	public function index(){
		$this->load->model('Cus_wishlist_m');
		$data['list']=$this->Cus_wishlist_m->load_all_Cus_wishlist();
		// print_r($data['list']);return;
		$this->load->view('admin/a_cus_wishlist',$data);
	}
	public function add_cus_wishlist()
	{
		$this->load->view('admin/a_cus_wishlist_add');
	}
	public function form_add_cus_wishlist($userId, $productId, $type){
		$userId = $this->input->post('userId');
		$productId = $this->input->post('productId');
		$type = $this->input->post('type');
		
		$this->load->model('Cus_wishlist_m');
		$this->Cus_wishlist_m->add_cus_wishlist($userId, $productId, $type);
		redirect('/admin/cus_wishlist/');
	}
	public function edit_cus_wishlist($id)
	{
		$this->load->model('Cus_wishlist_m');
		$data['list']=$this->Cus_wishlist_m->get_cus_wishlist_by_id($id);
		$this->load->view('admin/a_cus_wishlist_edit',$data);
	}
	public function form_edit_cus_wishlist($userId, $productId, $type){
		$id = $this->input->post('$id');
		$userId = $this->input->post('userId');
		$productId = $this->input->post('productId');
		$type = $this->input->post('type');
		
		$this->load->model('Cus_wishlist_m');
		$this->Cus_wishlist_m->edit_cus_wishlist_by_id($id, $userId, $productId, $type);
		redirect('/admin/cus_wishlist/');
	}
	public function delete_cus_wishlist($id)
	{
		$this->load->model('Cus_wishlist_m');
		$data['list']=$this->Cus_wishlist_m->delete_cus_wishlist_by_id($id);
		redirect('/admin/cus_wishlist/');
	
	}
}