<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
	
	public function index(){
		$this->load->model('Customer_m');
		$data['list']=$this->Customer_m->load_all_Customer();
		// print_r($data['list']);return;
		$this->load->view('admin/a_customer',$data);
	}
	public function add_customer()
	{
		$this->load->view('admin/a_customer_add');
	}
	public function form_add_customer($fullname, $email, $phone, $birthday, $gender, $score, $idLevel, $password, $avatar){
		$fullname = $this->input->post('fullname');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$birthday = $this->input->post('birthday');
		$gender = $this->input->post('gender');
		$score = $this->input->post('score');
		$idLevel = $this->input->post('idLevel');
		$password = $this->input->post('password');
		$avatar = $this->input->post('avatar');
		
		$this->load->model('Customer_m');
		$this->Customer_m->add_customer($fullname, $email, $phone, $birthday, $gender, $score, $idLevel, $password, $avatar);
		redirect('/admin/customer/');
	}
	public function edit_customer($id)
	{
		$this->load->model('Customer_m');
		$data['list']=$this->Customer_m->get_customer_by_id($id);
		$this->load->view('admin/a_customer_edit',$data);
	}
	public function form_edit_customer($fullname, $email, $phone, $birthday, $gender, $score, $idLevel, $password, $avatar){
		$id = $this->input->post('$id');
		$fullname = $this->input->post('fullname');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$birthday = $this->input->post('birthday');
		$gender = $this->input->post('gender');
		$score = $this->input->post('score');
		$idLevel = $this->input->post('idLevel');
		$password = $this->input->post('password');
		$avatar = $this->input->post('avatar');
		
		$this->load->model('Customer_m');
		$this->Customer_m->edit_customer_by_id($id, $fullname, $email, $phone, $birthday, $gender, $score, $idLevel, $password, $avatar);
		redirect('/admin/customer/');
	}
	public function delete_customer($id)
	{
		$this->load->model('Customer_m');
		$data['list']=$this->Customer_m->delete_customer_by_id($id);
		redirect('/admin/customer/');
	
	}
}