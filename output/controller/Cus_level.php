<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cus_level extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
	
	public function index(){
		$this->load->model('Cus_level_m');
		$data['list']=$this->Cus_level_m->load_all_Cus_level();
		// print_r($data['list']);return;
		$this->load->view('admin/a_cus_level',$data);
	}
	public function add_cus_level()
	{
		$this->load->view('admin/a_cus_level_add');
	}
	public function form_add_cus_level($title, $description, $requestScore, $action){
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$requestScore = $this->input->post('requestScore');
		$action = $this->input->post('action');
		
		$this->load->model('Cus_level_m');
		$this->Cus_level_m->add_cus_level($title, $description, $requestScore, $action);
		redirect('/admin/cus_level/');
	}
	public function edit_cus_level($id)
	{
		$this->load->model('Cus_level_m');
		$data['list']=$this->Cus_level_m->get_cus_level_by_id($id);
		$this->load->view('admin/a_cus_level_edit',$data);
	}
	public function form_edit_cus_level($title, $description, $requestScore, $action){
		$id = $this->input->post('$id');
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$requestScore = $this->input->post('requestScore');
		$action = $this->input->post('action');
		
		$this->load->model('Cus_level_m');
		$this->Cus_level_m->edit_cus_level_by_id($id, $title, $description, $requestScore, $action);
		redirect('/admin/cus_level/');
	}
	public function delete_cus_level($id)
	{
		$this->load->model('Cus_level_m');
		$data['list']=$this->Cus_level_m->delete_cus_level_by_id($id);
		redirect('/admin/cus_level/');
	
	}
}