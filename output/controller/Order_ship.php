<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_ship extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
	
	public function index(){
		$this->load->model('Order_ship_m');
		$data['list']=$this->Order_ship_m->load_all_Order_ship();
		// print_r($data['list']);return;
		$this->load->view('admin/a_order_ship',$data);
	}
	public function add_order_ship()
	{
		$this->load->view('admin/a_order_ship_add');
	}
	public function form_add_order_ship($idOrder, $idsipMethod, $paymentMethodId, $shipFee, $toltalFee, $message){
		$idOrder = $this->input->post('idOrder');
		$idsipMethod = $this->input->post('idsipMethod');
		$paymentMethodId = $this->input->post('paymentMethodId');
		$shipFee = $this->input->post('shipFee');
		$toltalFee = $this->input->post('toltalFee');
		$message = $this->input->post('message');
		
		$this->load->model('Order_ship_m');
		$this->Order_ship_m->add_order_ship($idOrder, $idsipMethod, $paymentMethodId, $shipFee, $toltalFee, $message);
		redirect('/admin/order_ship/');
	}
	public function edit_order_ship($id)
	{
		$this->load->model('Order_ship_m');
		$data['list']=$this->Order_ship_m->get_order_ship_by_id($id);
		$this->load->view('admin/a_order_ship_edit',$data);
	}
	public function form_edit_order_ship($idOrder, $idsipMethod, $paymentMethodId, $shipFee, $toltalFee, $message){
		$id = $this->input->post('$id');
		$idOrder = $this->input->post('idOrder');
		$idsipMethod = $this->input->post('idsipMethod');
		$paymentMethodId = $this->input->post('paymentMethodId');
		$shipFee = $this->input->post('shipFee');
		$toltalFee = $this->input->post('toltalFee');
		$message = $this->input->post('message');
		
		$this->load->model('Order_ship_m');
		$this->Order_ship_m->edit_order_ship_by_id($id, $idOrder, $idsipMethod, $paymentMethodId, $shipFee, $toltalFee, $message);
		redirect('/admin/order_ship/');
	}
	public function delete_order_ship($id)
	{
		$this->load->model('Order_ship_m');
		$data['list']=$this->Order_ship_m->delete_order_ship_by_id($id);
		redirect('/admin/order_ship/');
	
	}
}