<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_info extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
	
	public function index(){
		$this->load->model('Order_info_m');
		$data['list']=$this->Order_info_m->load_all_Order_info();
		// print_r($data['list']);return;
		$this->load->view('admin/a_order_info',$data);
	}
	public function add_order_info()
	{
		$this->load->view('admin/a_order_info_add');
	}
	public function form_add_order_info($idOrder, $rewardScore, $idtrackingOrder){
		$idOrder = $this->input->post('idOrder');
		$rewardScore = $this->input->post('rewardScore');
		$idtrackingOrder = $this->input->post('idtrackingOrder');
		
		$this->load->model('Order_info_m');
		$this->Order_info_m->add_order_info($idOrder, $rewardScore, $idtrackingOrder);
		redirect('/admin/order_info/');
	}
	public function edit_order_info($id)
	{
		$this->load->model('Order_info_m');
		$data['list']=$this->Order_info_m->get_order_info_by_id($id);
		$this->load->view('admin/a_order_info_edit',$data);
	}
	public function form_edit_order_info($idOrder, $rewardScore, $idtrackingOrder){
		$id = $this->input->post('$id');
		$idOrder = $this->input->post('idOrder');
		$rewardScore = $this->input->post('rewardScore');
		$idtrackingOrder = $this->input->post('idtrackingOrder');
		
		$this->load->model('Order_info_m');
		$this->Order_info_m->edit_order_info_by_id($id, $idOrder, $rewardScore, $idtrackingOrder);
		redirect('/admin/order_info/');
	}
	public function delete_order_info($id)
	{
		$this->load->model('Order_info_m');
		$data['list']=$this->Order_info_m->delete_order_info_by_id($id);
		redirect('/admin/order_info/');
	
	}
}